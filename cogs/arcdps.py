import datetime
from typing import Optional
import bs4

import discord
import requests
from discord.ext import commands, tasks
from config import cfg
from discord_bot import logger

ARCDPS_DIR = "https://www.deltaconnected.com/arcdps/x64/"
ARCDPS_CHECKSUM = "https://www.deltaconnected.com/arcdps/x64/d3d9.dll.md5sum"

if "arcdps" not in cfg:
    cfg["arcdps"] = {"channel": None, "md5sum": None}


class ArcdpsWatch(commands.Cog):

    def __init__(self, bot):
        self._bot = bot
        self.check.start()

    @property
    def channel(self) -> Optional[discord.TextChannel]:
        if cfg["arcdps"]["channel"] is not None:
            return self._bot.get_channel(cfg["arcdps"]["channel"])

    async def cog_check(self, ctx):
        return await self._bot.is_owner(ctx.author)

    def cog_unload(self):
        self.check.cancel()

    @commands.command(hidden=True)
    async def set_arcdps_channel(self, ctx: commands.Context, channel: Optional[discord.TextChannel] = None):
        channel = ctx.channel if channel is None else channel
        cfg["arcdps"]["channel"] = channel.id
        await cfg.save()
        await ctx.send(f"Setting the output for notifications for arcdps updates to {channel.name}")

    @tasks.loop(minutes=5)
    async def check(self):
        old, new = cfg["arcdps"]["md5sum"], requests.get(ARCDPS_CHECKSUM).text
        if old != new and self.channel is not None:
            dt = datetime.datetime.strptime(bs4.BeautifulSoup(requests.get(ARCDPS_DIR).text, features="lxml").
                                            body.table.contents[7].contents[2].string.rstrip(), "%Y-%m-%d %H:%M")
            await self.channel.send(f"A new version of arcdps is available.  Link to download: {ARCDPS_DIR}\n"
                                    f"Date: {dt}\n"
                                    f"MD5: {new.split(' ', 1)[0]}")
            cfg["arcdps"]["md5sum"] = new
            await cfg.save()
            logger.info("ArcdpsWatch config saved.")


def setup(bot):
    bot.add_cog(ArcdpsWatch(bot))


def teardown(bot):
    pass
