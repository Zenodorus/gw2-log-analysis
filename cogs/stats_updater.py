import argparse
import datetime
import json
from typing import Dict, Union

import discord
from discord.ext import commands

from util import DatetimeConverter
from config import cfg
from discord_bot import logger
from util.ei.enum import BOON_IDS, BALANCE_PATCHES
from util.ei.enum.encounter import ENCOUNTER_NAMES
from util.ei.enum.specialization import BUILDS, PROFESSIONS
from util.ei.log import EliteInsightLog, build_log, RE_DPS_REPORT
from util.ei.log_cache import LogCache
from util.ei.log_collection import SessionLogCollection
from util.ei.util import week_number, hms

# with open("orly/config/session_stats_updater.json", 'r') as _f:
#     config = json.load(_f)


if "stats_updater" not in cfg:
    cfg["stats_updater"] = {}
cog_cfg = cfg["stats_updater"]


target_names = {"cairn": "Cairn the Indomitable",
                41: "Cairn the Indomitable",
                "sh": "Soulless Horror",
                51: "Soulless Horror"}


def group_quoted(args):
    i = 0
    in_single = in_double = None
    while i < len(args):
        if in_single is not None:
            args[in_single] += ' ' + args.pop(i)
            if args[in_single].endswith("'"):
                args[in_single] = args[in_single][1:-1]
                in_single = None
        elif in_double is not None:
            args[in_double] += ' ' + args.pop(i)
            if args[in_double].endswith('"'):
                args[in_double] = args[in_double][1:-1]
                in_double = None
        elif args[i].startswith('"'):
            in_double = i
            i += 1
        elif args[i].startswith("'"):
            in_single = i
            i += 1
        else:
            i += 1
    return args


class ParseError(Exception):

    def __init__(self, message):
        self.message = message


class DoNotExitParser(argparse.ArgumentParser):

    def error(self, message):
        """error(message: string)

        Prints a usage message incorporating the message to stderr and
        exits.

        If you override this in a subclass, it should not return -- it
        should either exit or raise an exception.
        """
        raise ParseError(message=self.format_usage())
        # self.print_usage(sys.stderr)
        # args = {'prog': self.prog, 'message': message}
        # self._print_message(_('%(prog)s: error: %(message)s\n') % args, sys.stderr)


class SessionStatsUpdater(commands.Cog):

    ALLOWED_ROLES = {452667736162566144: (632650604593938477, 632651659490623508),
                     559906719153651724: (560129402042580992,),
                     839333826169536533: (839333826253553679, 839333826253553678),
                     }

    def __init__(self, bot):
        self._bot = bot

    @property
    def log_cache(self) -> LogCache:
        return self._bot.log_cache

    async def save_config(self):
        await cfg.save()
        logger.info("SessionStatsUpdater config saved.")

    async def cog_check(self, ctx):
        return (await self._bot.is_owner(ctx.author)) or ctx.author.guild_permissions.administrator

    async def cog_command_error(self, ctx, error):
        if isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.send(str(error.args[0]))
        else:
            await ctx.send(str(error))

    # @commands.command()
    # @commands.is_owner()
    # async def analysis(self, ctx: commands.Context, *, args):
    #     ap = DoNotExitParser(exit_on_error=False)
    #     ap.add_argument('link')
    #     ap.add_argument('-b', '--boon')
    #     ap.add_argument('-t', '--target', nargs='?')
    #     ap.add_argument('-p', '--player', default=False)
    #     args = ap.parse_args(group_quoted(args.split(' ')))
    #
    #     if args.target and args.player:
    #         await ctx.send("Player and target arguments cannot be both specified")
    #         return
    #
    #     m = await ctx.send("Downloading log data")
    #     log = EliteInsightLog.from_hyperlink(args.link)
    #     name = args.target or args.player
    #     is_player = bool(args.player)
    #     start, end = log.data["phases"][0]["start"], log.data["phases"][0]["end"]
    #     name = target_names.get(name, name) or (log.data["targets"][0]["name"] if not is_player else name)
    #     args.boon = args.boon.split(',')
    #     out = tuple(log.ttl_buff_stacks_by_time(name, is_player, BOON_IDS[b], start, end) for b in args.boon)
    #     content = '\n'.join(f"{name} has {round(out[i], 2)} stack-seconds of {args.boon[i]}, with an average of "
    #                         f"{round(out[i] / (end - start), 2)} stacks." for i in range(len(out)))
    #     await m.edit(content=content)
    #
    # @analysis.error
    # async def analysis_error(self, ctx: commands.Context, error):
    #     if isinstance(error, commands.CommandInvokeError):
    #         if isinstance(error.original, ParseError):
    #             await ctx.send(error.original.message)
    #         else:
    #             await ctx.send("not a ParseError")
    #     else:
    #         await ctx.send("something happened")

    @commands.command()
    async def pb_channel(self, ctx: commands.Context, tag: str, arg: Union[int, str]):
        if tag not in cog_cfg:
            cog_cfg[tag] = {}
        if isinstance(arg, int):
            cog_cfg[tag][f"w{arg}_channel"] = ctx.channel.id
            await ctx.send(f'Channel {ctx.channel.name} set for wing {arg} PBs for tag "{tag}"', delete_after=5)
        elif arg == "session":
            cog_cfg[tag][f"session_channel"] = ctx.channel.id
            await ctx.send(f'Channel {ctx.channel.name} set for session PBs for tag "{tag}"', delete_after=5)
        await ctx.message.delete()
        await self.save_config()

    async def _compute_encounter_pb(self, tag: str, eid: int, num_logs: int = 4):
        wid = int(eid / 10)
        if f"w{wid}_channel" in cog_cfg[tag]:
            channel: discord.channel.TextChannel = self._bot.get_channel(cog_cfg[tag][f"w{wid}_channel"])
            logs = self.log_cache.filter_by_encounter(tag=tag, enc=eid, result=True)
            if f"e{eid}_message" not in cog_cfg[tag]:
                emsg = await channel.send(embed=logs.recent_logs(1, 1 + num_logs).report(logs.latest_log))
                cog_cfg[tag][f"e{eid}_message"] = emsg.id
            else:
                emsg = await channel.fetch_message(cog_cfg[tag][f"e{eid}_message"])
                await emsg.edit(embed=logs.recent_logs(1, 1 + num_logs).report(logs.latest_log))

    @commands.command()
    async def compute_pbs(self, ctx: commands.Context, tag: str):
        """
        Compute the personal bests for logs with given tag.

        TODO: currently this command focuses only on logs from the current meta (as of this writing,
        balance patch 2021/05/11).  Need to add optional date argument, and add a command to display
        list of balance patches

        :param tag: tag on the logs
        """
        # all time PBs
        # session_times = monday_logs.session_list(
        #     start=datetime.datetime(year=2020, month=4, day=24, tzinfo=datetime.timezone.utc))
        # current meta
        # session_weeks = self.log_cache.session_list(tag=tag)
        session_weeks = self.log_cache.session_list(tag=tag, min_build=BALANCE_PATCHES["20210511"])
        logs: Dict[datetime.datetime, SessionLogCollection] = {w: self.log_cache.filter_by_week(tag, w)
                                                               for w in session_weeks}

        if not logs:
            await ctx.send("No logs exist with this tag in the specified time period")
            return

        ENCOUNTER_ORDER = [13, 14, 11, 32, 33, 34, 21, 22, 23, 41, 42, 43, 44, 71, 72, 73, 61, 62, 63, 51, 52, 53,
                           54, 55, 56]
        SPLITS = ENCOUNTER_ORDER.copy()
        for i in range(len(SPLITS) - 2, -1, -1):
            SPLITS.insert(i+1, (SPLITS[i], SPLITS[i+1]))

        splits = {s: [] for s in SPLITS}

        for n in logs:
            sd = logs[n].date
            for s, t in logs[n].split_times.items():
                if s in splits:
                    splits[s].append((t, sd))

        # splits = {sid: [] for sid in SessionLogCollection.SPLIT_IDS}
        wings = {w: [] for w in range(1, 8)}
        fail_times = []
        durations = []
        down_state = []
        deaths = []
        resurrection_rate = []
        error = []

        msg = await ctx.send("Processing logs...")

        async def _load_session(n):
            with logs[n] as session_logs:
                st = session_logs.date
                if not session_logs.is_complete_fc:
                    missing = map(ENCOUNTER_NAMES.__getitem__, session_logs.missing_logs)
                    await msg.edit(content=f'{msg.content}\n{(st.astimezone(tz=None)).strftime("%Y-%m-%d")} '
                                           f'is missing log(s) for {", ".join(missing)}')
                    error.append(True)
                    return
                # for sid in splits:
                #     splits[sid].append((session_logs.encounter_time(sid), st))
                for w in wings:
                    wings[w].append((session_logs.wing_time(w), st))
                fail_times.append((session_logs.failed_combat_time, session_logs.num_fail_logs, st))
                durations.append((session_logs.duration.total_seconds(), st))
                dst = session_logs.down_state_time
                down_state.append((dst, st))
                rr = session_logs.resurrect_time / dst
                if rr < 1:
                    resurrection_rate.append((session_logs.resurrect_time / dst, st))
                deaths.append((session_logs.deaths, st))
                await msg.edit(content=f'{msg.content}\n{(st.astimezone(tz=None)).strftime("%Y-%m-%d")} processed')

        for w in logs:
            await _load_session(w)

        if not durations:
            return

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        theoretical_best = 0
        combat_best = 0
        for k, v in splits.items():
            v.sort()
            pb, date = v[0]
            is_new = (now - date).total_seconds() < 3600 * 24 * 7 * 4
            splits[k] = (f'{"**" if is_new else ""}{ENCOUNTER_NAMES[k]}: __{hms(pb)}__ '
                         f'([{(date.astimezone(tz=None)).strftime("%Y-%m-%d")}]'
                         f'({logs[week_number(date)].encounter(k).url if isinstance(k, int) else ""}))'
                         f'{"**" if is_new else ""}\n')
            theoretical_best += pb
            combat_best += pb if isinstance(k, int) else 0

        wing_total_best = 0
        for k, v in wings.items():
            v.sort()
            pb, date = v[0]
            is_new = (now - date).total_seconds() < 3600 * 24 * 7 * 4
            wings[k] = (f'{"**" if is_new else ""}Full Wing: __{hms(pb)}__ '
                        f'({(date.astimezone(tz=None)).strftime("%Y-%m-%d")}){"**" if is_new else ""}')
            wing_total_best += pb

        embed = discord.Embed(title="Raid Encounter Personal Bests (Summer 2021 Meta)",
                              timestamp=datetime.datetime.utcnow(), colour=0xF08030)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[:5])}\n{wings[1]}', name="Wing 1", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[5:11])}\n{wings[3]}', name="Wing 3", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[11:17])}\n{wings[2]}', name="Wing 2", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[17:25])}\n{wings[4]}', name="Wing 4", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[25:31])}\n{wings[7]}', name="Wing 7", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[31:37])}\n{wings[6]}', name="Wing 6", inline=False)
        embed.add_field(value=f'{"".join(splits[k] for k in SPLITS[37:])}\n{wings[5]}', name="Wing 5", inline=False)
        embed.set_thumbnail(url="https://wiki.guildwars2.com/images/5/5e/Legendary_Insight.png")

        # print(len(embed))

        channel: discord.channel.TextChannel = self._bot.get_channel(cog_cfg[tag]["session_channel"])
        if "pb_message" not in cog_cfg[tag]:
            pb_message = await channel.send(embed=embed)
            cog_cfg[tag]["pb_message"] = pb_message.id
        else:
            pb_message = await channel.fetch_message(cog_cfg[tag]["pb_message"])
            await pb_message.edit(embed=embed)

        data = [sorted(durations)[0], sorted(fail_times)[0], sorted(fail_times, key=lambda x: (x[1], x[0]))[0],
                sorted(down_state)[0], sorted(resurrection_rate, reverse=True)[0], sorted(deaths)[0]]
        format_lines = [f"Session Duration: {hms(data[0][0])} ($date)",
                        f"Least Failed Combat Time: {hms(data[1][0])} ($date, {data[1][1]} failed logs)",
                        f"Fewest Failed Encounters: {data[2][1]} ($date, {hms(data[2][0])} time wasted)",
                        f"Least Downed State Time: {hms(data[3][0])} ($date)",
                        f"Highest F to Resurrect Rate: {100 * data[4][0]:.3f}% ($date)",
                        f"Fewest Deaths*: {data[5][0]} ($date)"]
        for i in range(len(format_lines)):
            format_lines[i] = format_lines[i].replace("$date", "{date}").format(
                date=data[i][-1].astimezone(tz=None).strftime("%Y/%m/%d"))
            if (now - data[i][-1]).total_seconds() < 3600 * 24 * 7 * 4:
                format_lines[i] = f"**{format_lines[i]}**"
        content = (f"Session PBs for Rockin' Revenants Meta (all combat excludes escort)\n\n"
                   f"Sum of Encounter PBs: {hms(theoretical_best)}\n"
                   f"Sum of Full Wing PBs: {hms(wing_total_best)}\n"
                   f"Sum of Combat PBs: {hms(combat_best)}\n"
                   f"Combat Time Ratio: {100 * combat_best / theoretical_best:0.3f}%\n\n")
        content += '\n'.join(format_lines)
        content += ("\n\n* failed logs always add 10 deaths, and no death context included "
                    "(e.g. sacrifices to Samarog and Deimos are counted)")

        if "session_message" not in cog_cfg[tag]:
            session_message = await channel.send(content=content)
            cog_cfg[tag]["session_message"] = session_message.id
        else:
            session_message = await channel.fetch_message(cog_cfg[tag]["session_message"])
            await session_message.edit(content=content)

        if "w6_channel" in cog_cfg[tag]:
            channel: discord.channel.TextChannel = self._bot.get_channel(cog_cfg[tag]["w6_channel"])
            elogs = self.log_cache.filter_by_encounter(tag=tag, enc=63, result=True)
            if "e63_message" not in cog_cfg[tag]:
                emsg = await channel.send(content=elogs.recent_logs(1, 5).report(elogs.latest_log))
                cog_cfg[tag]["e63_message"] = emsg.id
            else:
                emsg = await channel.fetch_message(cog_cfg[tag]["e63_message"])
                await emsg.edit(content=elogs.recent_logs(1, 5).report(elogs.latest_log))

        await self._compute_encounter_pb(tag, 13)
        await self._compute_encounter_pb(tag, 21)
        await self._compute_encounter_pb(tag, 32)
        await self._compute_encounter_pb(tag, 33)
        await self._compute_encounter_pb(tag, 61)
        await self._compute_encounter_pb(tag, 72)
        await self._compute_encounter_pb(tag, 73)

        await self.save_config()
        await ctx.message.delete()
        if not error:
            await msg.delete()

    # @commands.command()
    # async def once(self, ctx: commands.Context):
    #     channel: discord.channel.TextChannel = self._bot.get_channel(config["session_channel"])
    #     message = await channel.fetch_message(850564439169040384)
    #     await message.delete()
    #     await ctx.message.delete()

    @commands.command()
    async def list_players(self, ctx: commands.Context, tag: str, date: str):
        """
        List all the players from the specified session.

        :param tag: tag on the logs
        :param date: any date during the week formatted as "YYYY-MM-DD"
        """
        logs = self.log_cache.filter_by_week(tag, datetime.datetime.strptime(date, "%Y-%m-%d"))
        await ctx.send('\n'.join(f"{k} ({v} log{'s' if v > 1 else ''})" for k, v in logs.all_players.items()))

    @commands.command()
    async def report(self, ctx: commands.Context, tag: str, enc: int):
        logs = self.log_cache.filter_by_encounter(tag=tag, enc=enc, result=True)
        report = logs.recent_logs(1, 5).report(logs.latest_log)
        if isinstance(report, str):
            await ctx.channel.send(content=report)
        elif isinstance(report, discord.Embed):
            await ctx.channel.send(embed=report)

    @commands.command()
    async def compare(self, ctx: commands.Context, tag: str, log: str):
        msg = await ctx.send("Loading log data...")
        ei_log = build_log(log)
        logs = self.log_cache.filter_by_encounter(tag=tag, enc=ei_log.encounter_id, result=True)
        report = logs.recent_logs(0, 4).report(ei_log)
        if isinstance(report, str):
            await msg.edit(content=report)
        elif isinstance(report, discord.Embed):
            await msg.edit(content=None, embed=report)

    @commands.command()
    async def mechanics(self, ctx: commands.Context, tag: str):
        logs = self.log_cache.filter_by_encounter(tag=tag, enc=56, result=True)
        logs.latest_log.arcing_affliction()

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if not await self._bot.is_owner(message.author) or message.content.startswith(self._bot.command_prefix):
            return

        # args = message.content
        # logs = []
        # while m := re.search(r"https?://", args):
        #     if link := RE_DPS_REPORT.match(args, m.start()) or RE_DISCORD_ATTACH.match(args, m.start()):
        #         args = args[link.end():]
        #         logs.append(link.group())
        #     else:
        #         args = args[m.end():]
        s = RE_DPS_REPORT.search(message.content)
        if s is not None:
            if (s.start() and s.end() < len(message.content) and
                    message.content[s.start() - 1] == '[' and message.content[s.end()] == ']'):
                return
            if s.group() in self.log_cache:
                log = self.log_cache[s.group()]
            else:
                log = EliteInsightLog(link=s.group(), metadata=None, do_cache=False)
            await message.channel.send(embed=log.embed)
            return

        if message.attachments:
            url = message.attachments[0].url
            if url in self.log_cache:
                log = self.log_cache[url]
            else:
                log = EliteInsightLog(link=url, metadata=None, do_cache=False)
            await message.channel.send(embed=log.embed)


    @commands.command()
    async def role_review(self, ctx: commands.Context, tag: str, *, args: str = ""):
        if not args:
            args = datetime.datetime.now()
        else:
            raise NotImplemented
        logs = self.log_cache.filter_by_week(tag, args)
        for log in logs.success_logs:
            await ctx.send(embed=log.embed)

    @commands.command()
    async def test(self, ctx: commands.Context):
        await ctx.send(f"\\:weaver:")

    @commands.command(hidden=True)
    async def build(self, ctx: commands.Context, *, build: str):
        for b in BUILDS:
            if b["name"].lower() == build.lower():
                required_specs = tuple(filter(None, (line["specialization"] for line in b["required"].traits)))
                required_traits = tuple(filter(None, (t for line in b["required"].traits for t in line["major"])))
                required_skills = tuple(filter(None, b["required"].skills))

                embed = discord.Embed(title=f'{b["name"]} Build',
                                      description="Underline indicates the elements that are required to identify this "
                                                  "build in a log.")
                embed.add_field(name="Role", value=b["role"], inline=True)
                embed.add_field(name="Profession", value=b["profession"], inline=True)
                if b["required"].damage == b["build"].damage:
                    embed.add_field(name="Damage Type", value=f"__{b['build'].damage}__", inline=True)
                else:
                    embed.add_field(name="Damage Type", value=b['build'].damage, inline=True)

                value = ""
                for line in b["build"].traits:
                    spec, traits = line['specialization'], line['major']
                    value += f"\n__{spec.name}__" if spec in required_specs else f"\n{spec.name}"
                    for t in traits:
                        value += (f"\n- __{t.name}__ ({t.order + 1})" if t in required_traits
                                  else f"\n- {t.name} ({t.order + 1})")

                embed.add_field(name="Traits", inline=True, value=value)
                value = ""
                for skill in b["build"].skills:
                    if skill is not None:
                        value += f"\n__{skill.name}__" if skill in required_skills else f"\n{skill.name}"
                    else:
                        value += "\n*Unspecified*"
                embed.add_field(name="Skills", inline=True, value=value)

                if b.get("template", None):
                    embed.add_field(name="Build Template", value=b["template"], inline=False)

                eid = PROFESSIONS[b["profession"]].emoji.split(':')[2][:-1]
                embed.set_thumbnail(url=f"https://cdn.discordapp.com/emojis/{eid}.png")
                await ctx.send(embed=embed)

                return
        await ctx.send(f'Build "{build}" not found.')

    @commands.command(hidden=True)
    async def list_builds(self, ctx: commands.Context):
        await ctx.send('\n'.join(b["name"] for b in BUILDS))

    @commands.command(hidden=True)
    async def find_builds(self, ctx: commands.Context, tag: str, enc: int, *, build: str):
        for b in BUILDS:
            if b["name"].lower() == build:
                build = b
                break
        else:
            await ctx.send(f'Build "{build}" not found.')
            return

        results = {}
        for log in self.log_cache.filter_by_encounter(tag, enc, result=True).recent_logs(a=0, b=10).logs:
            for p in map(log.players.get, range(log.num_players)):
                if p.exact_build == build:
                    link = f"{log.strf_date} ({log.url})"
                    if link not in results:
                        results[link] = []
                    results[link].append(p.name)
        embed = discord.Embed(title=f'Search Results for "{build["name"]}"')
        for k in sorted(results):
            embed.add_field(name=k, value='\n'.join(results[k]), inline=False)
        eid = PROFESSIONS[build["profession"]].emoji.split(':')[2][:-1]
        embed.set_thumbnail(url=f"https://cdn.discordapp.com/emojis/{eid}.png")

        await ctx.send(embed=embed)

    @commands.command(hidden=True)
    async def week_number(self, ctx: commands.Context, date: Union[DatetimeConverter, str] = ""):
        if isinstance(date, datetime.datetime):
            await ctx.send(f"The week number for {date.isoformat(sep=' ')} is {week_number(date)}.")
        else:
            await ctx.send(f"The current week number is {week_number(datetime.datetime.now())}.")


def setup(bot):
    bot.add_cog(SessionStatsUpdater(bot))


def teardown(bot):
    pass
