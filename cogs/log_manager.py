import asyncio
import datetime
import json
import os
import re
from typing import Optional, Union

import discord
from discord.ext import commands

from discord_bot import DiscordBot
from util import DatetimeConverter, DateTimeDecoder, DateTimeEncoder
from util.ei.enum import BUFF_IDS, save_mappings
from util.ei.enum.encounter import ENCOUNTERS
from util.ei.log import EliteInsightLog, RE_DPS_REPORT, RE_DISCORD_ATTACH
from util.ei.log_cache import LogCache, LogConflict
from util.ei.util import week_start_datetime, week_number, join_max_size


class LogManager(commands.Cog):
    """
    Cog to manage a cache of raid logs to provide statistics and analysis.
    """

    ALLOWED_ROLES = {452667736162566144: (632650604593938477, 632651659490623508),
                     559906719153651724: (560129402042580992,),
                     839333826169536533: (839333826253553679, 839333826253553678),
                     }

    def __init__(self, bot: DiscordBot):
        self._bot: DiscordBot = bot

    @property
    def log_cache(self) -> LogCache:
        return self._bot.log_cache

    async def cog_check(self, ctx: commands.Context):
        if await ctx.bot.is_owner(ctx.author):
            return True

        for role in map(ctx.guild.get_role, self.ALLOWED_ROLES[ctx.guild.id]):
            if role in ctx.author.roles:
                return True

        return False

    # async def cog_command_error(self, ctx, error):
    #     if isinstance(error, commands.errors.MissingRequiredArgument):
    #         await ctx.send(str(error.args[0]))
    #
    # @commands.command(hidden=True)
    # @commands.is_owner()
    # async def build_cache_from_manager(self, ctx: commands.Context, tag: str, max_num: Optional[int] = 0xFFFFFFFF):
    #     """
    #     Load the cache from Arcdps Log Manager and pull logs with the specified tag.
    #
    #     :param tag: tag on the logs entered in Arcdps Log Manager
    #     :param max_num: Maximum number of logs to be downloaded from dps.report before stopped.
    #     """
    #     with open("/home/dan/.local/share/ArcdpsLogManager/LogDataCache.json", 'rb') as f:
    #         log_manager_cache = json.loads(f.read().decode("utf-8-sig"))["LogsByFilename"]
    #
    #     tag = {"Name": tag}
    #     errored_logs = []
    #     not_uploaded = []
    #     not_cached = []
    #     keys = ("Encounter", "EncounterMode", "EncounterResult", "EncounterStartTime", "EncounterDuration",
    #             "DpsReportEIUpload", "FileName", "PointOfView", "Tags", "GameBuild")
    #     for filename in tuple(log_manager_cache):
    #         if tag not in log_manager_cache[filename]["Tags"]:
    #             del log_manager_cache[filename]
    #             continue
    #
    #         log = {k: v for (k, v) in log_manager_cache[filename].items() if k in keys}
    #         log["EncounterStartTime"] = datetime.datetime.fromisoformat(log["EncounterStartTime"])
    #         h, m, s = map(float, log["EncounterDuration"].split(':'))
    #         log["EncounterDuration"] = h * 3600 + m * 60 + s
    #         url = log["DpsReportEIUpload"]["Url"]
    #         if url is None:
    #             if log["DpsReportEIUpload"]["ProcessingError"]:
    #                 errored_logs.append(log)
    #             not_uploaded.append(log)
    #         else:
    #             log["FileName"] = re.match(r"https?://dps[.]report/(\w{4}-\d{8}-\d{6}_\w+)", url).group(1) + ".json.gz"
    #             if url not in log_cache or not log_cache[url]["DataCached"]:
    #                 log["DataCached"] = False
    #                 not_cached.append(log)
    #             else:
    #                 log["DataCached"] = True
    #                 ei_log = EliteInsightLog(url, metadata=log, do_cache=False)
    #                 log_cache[url] = ei_log.metadata
    #
    #     await ctx.send(f'Found {len(log_manager_cache)} logs in the log manager cache with the tag "{tag}", '
    #                    f'{len(not_uploaded)} logs have not been uploaded to dps.report (of which '
    #                    f'{len(errored_logs)} were erroneous logs), and '
    #                    f'{len(not_cached)} are uploaded but not present in the bot cache.')
    #
    #     # prepare the metadata from each log that has been uploaded but does not have a local data cache
    #
    #     msg = await ctx.send(f"Downloading data from dps.report.  0 / {len(not_cached)} complete.")
    #     try:
    #         for i, metadata in enumerate(not_cached):
    #             url = metadata["DpsReportEIUpload"]["Url"]
    #             do_cache = (i < max_num)
    #             ei_log = EliteInsightLog(url, metadata=metadata, do_cache=do_cache)
    #             if not ei_log.metadata["DataCached"] and do_cache:
    #                 if do_cache:
    #                     logger.info(f"Something went wrong trying to get data from {url}.  Searching the local cache")
    #                 else:
    #                     logger.info(f"Reached limit on downloads.  Searching the local cache for {url}")
    #                 old_name = re.match(r"https://dps.report/\w{4}-(\d{8}-\d{6})_\w+", url).group(1) + ".json"
    #                 found = True
    #                 try:
    #                     with open(os.path.join("logstats", "logdata", old_name), 'r') as f:
    #                         with gzip.open(os.path.join(LOGDATA_DIR, ei_log.filename), 'wt', encoding='UTF-8') as zf:
    #                             json.dump(json.load(f), zf)
    #                 except FileNotFoundError:
    #                     found = False
    #                 try:
    #                     with open(os.path.join("logstats", "graphdata", old_name), 'r') as f:
    #                         with gzip.open(os.path.join(GRAPHDATA_DIR, ei_log.filename), 'wt', encoding='UTF-8') as zf:
    #                             json.dump(json.load(f), zf)
    #                 except FileNotFoundError:
    #                     found = False
    #                 ei_log.metadata["DataCached"] = found
    #             log_cache[ei_log.url] = ei_log.metadata
    #
    #             if do_cache:
    #                 await msg.edit(content=f"Downloading data from dps.report.  "
    #                                        f"{i+1} / {len(not_cached)} complete.")
    #             elif i == max_num or i % 20 == 0:
    #                 await msg.edit(content=f"Downloading data from dps.report.  {i} / {len(not_cached)} complete.  "
    #                                        f"Reached max limit, so stopping downloads.")
    #     except Exception:
    #         await ctx.send(f"Something bad happened.  Saving progress to log cache.")
    #
    #     with open(LOG_CACHE_FILENAME, 'w') as f:
    #         json.dump(log_cache, f, cls=DateTimeEncoder, indent=2)
    #     await ctx.send("Cache updated.")
    #
    # @commands.command(hidden=True)
    # @commands.is_owner()
    # async def verify_data_cache(self, ctx: commands.Context):
    #     total = sum(1 for metadata in log_cache.values() if metadata["DataCached"])
    #     count = updated = 0
    #     bad_data = []
    #     await ctx.send(f"Found {total} logs with cached data.  Beginning fidelity check")
    #     msg = await ctx.send(f"{count} / {total} logs verified")
    #     for link, metadata in log_cache.items():
    #         if metadata["DataCached"]:
    #             ei_log = EliteInsightLog(link, metadata)
    #             try:
    #                 logdata, graphdata = ei_log.data, ei_log.graphdata
    #             except json.JSONDecodeError as e:
    #                 bad_data.append(f"{link} ({e.__class__.__name__})")
    #             except Exception as e:
    #                 bad_data.append(f"{link} ({e.__class__.__name__})")
    #             else:
    #                 count += 1
    #             if count % 20 == 0:
    #                 if bad_data:
    #                     links = '\n'.join(bad_data)
    #                     await msg.edit(content=f"{count} / {total} logs verified.  Found bad data from:\n{links}")
    #                 else:
    #                     await msg.edit(content=f"{count} / {total} logs verified.")
    #     if bad_data:
    #         links = '\n'.join(bad_data)
    #         await msg.edit(content=f"{count} / {total} logs verified.  Found bad data from:\n{links}")
    #     else:
    #         await msg.edit(content=f"{count} / {total} logs verified.")

    @commands.command()
    async def show_metadata(self, ctx: commands.Context, *, args):
        """
        Display the stored metadata for a given log, specified by a hyperlink.
        :param args: link to the log
        """
        if args not in self.log_cache:
            await ctx.send(f"{args} not found in cache")
            return

        metadata = self.log_cache[args].metadata
        await ctx.send(f"```{json.dumps(metadata, cls=DateTimeEncoder, indent=2)}```")

    @commands.command()
    async def add_log(self, ctx: commands.Context, tag: str, *, log_list: Union[discord.Message, str] = ""):
        """
        Add a set of logs to the manager with the specified tag.  Logs can be either a dps.report link or an
        html file generated by Elite Insights attached to the message.  Can add as many logs as can fit in
        one discord message.

        Ex:
        #!add_log monday https://dps.report/Fzve-20210913-185940_gors
        https://dps.report/uJq0-20210913-190444_sab
        https://dps.report/o6cD-20210913-190902_vg

        :param tag: alphanumeric tag assigned to the logs
        :param log_list: logs to be added to the cache or a link to a discord message containing dps.report links
        """
        if RE_DPS_REPORT.match(tag) or RE_DISCORD_ATTACH.match(tag):
            await ctx.send(f"First argument should be a tag to be assigned to the logs.")
            return
        elif re.match(r"\d+", tag):
            await ctx.send("Tags must start with an alphabetic character")
            return

        logs = []
        if ctx.message.attachments:
            if log_list:
                await ctx.send("Ignoring any arguments outside of the tag.  "
                               "Rerun command without attachment to process")
                return
            logs.append(ctx.message.attachments[0].url)

        if isinstance(log_list, discord.Message):
            log_list = log_list.content + '\n' + '\n'.join(str(e.to_dict()) for e in log_list.embeds)

        while m := re.search(r"https?://", log_list):
            if link := RE_DPS_REPORT.match(log_list, m.start()) or RE_DISCORD_ATTACH.match(log_list, m.start()):
                log_list = log_list[link.end():]
                logs.append(link.group())
            else:
                log_list = log_list[m.end():]

        progress = await ctx.send(f"Found {len(logs)} log{'s' if len(logs) > 1 else ''} to process.  Checking for "
                                  f"duplicates...")
        added = 0
        logs_ignored = []
        logs_removed = []
        for log in logs:
            try:
                self.log_cache.add_logs(log, tag)
            except LogConflict as e:
                # TODO display tags
                await ctx.send(f"The following two logs are in conflict.  Enter the number for which one to keep:\n"
                               f"(1) {e.existing_log.url} (existing log)\n(2) {e.new_log.url}")

                need_response = True
                while need_response:
                    need_response = False
                    try:
                        msg = await self._bot.wait_for("message", check=lambda _m: _m.author == ctx.author, timeout=120)
                    except asyncio.TimeoutError:
                        await ctx.send("Reply timeout reached.  Ignoring new log and proceeding")
                        logs_ignored.append(e.new_log.url)
                    else:
                        if msg.content.startswith('1'):
                            await ctx.send(f"Ignoring the new log {e.new_log.url}")
                            logs_ignored.append(e.new_log.url)
                        elif msg.content.startswith('2'):
                            await ctx.send(f"Removing the existing log {e.existing_log.url} and adding new log")
                            # TODO transfer tags
                            logs_removed.append(e.existing_log.url)
                            self.log_cache.remove_log(e.existing_log.url)
                            self.log_cache.add_logs(log, tag)
                            added += 1
                        else:
                            await ctx.send("Unknown response.  Please answer either '1' or '2'")
                            need_response = True
                progress = None
            else:
                added += 1
                if progress:
                    try:
                        await progress.edit(content=progress.content + f"\nAdded {log} to cache.")
                    except discord.errors.HTTPException:
                        progress = await ctx.send(f"Added {log} to cache.")
                else:
                    progress = await ctx.send(f"Added {log} to cache.")

        self.log_cache.save()
        output = f"{len(logs)} log{'s' if len(logs) > 1 else ''} added to the cache.  "
        if logs_ignored:
            output += "The following logs were ignored:\n" + '\n'.join(logs_ignored) + '\n'
        if logs_removed:
            output += "The following logs were removed from the cache:\n" + '\n'.join(logs_removed) + '\n'
        await ctx.send(output)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def update_id_mappings(self, ctx: commands.Context, *, args: str = ""):
        s = RE_DPS_REPORT.search(args)
        if s is None:
            await ctx.send("Provide a log to pull the IDs from")
            return

        if s.group() in self.log_cache:
            log = self.log_cache[s.group()]
        else:
            log = EliteInsightLog(link=s.group(), metadata=None, do_cache=False)

        n_buff = len(BUFF_IDS)
        BUFF_IDS.update((v["id"], v["name"]) for v in log.data["buffMap"].values())
        save_mappings()
        await ctx.send(f"Updated buffs dictionary with {len(BUFF_IDS) - n_buff} new buff(s).")
        # await message.channel.send(embed=log.embed)

    @commands.command()
    async def get_links(self, ctx: commands.Context, msg: discord.Message):
        content = msg.content + '\n' + '\n'.join(str(e.to_dict()) for e in msg.embeds)

        logs = []
        while m := re.search(r"https?://", content):
            if link := RE_DPS_REPORT.match(content, m.start()) or RE_DISCORD_ATTACH.match(content, m.start()):
                content = content[link.end():]
                logs.append(link.group())
            else:
                content = content[m.end():]

        await ctx.send('\n'.join(logs))

    @commands.command()
    async def log_summary(self, ctx: commands.Context, tag: str, date: Optional[DatetimeConverter] = None):
        if not date:
            output = {}
            for s in self.log_cache.session_list(tag):
                session = self.log_cache.filter_by_week(tag, s)
                dt = week_start_datetime(s)
                if dt.year not in output:
                    output[dt.year] = []
                line = f"Week of {dt.strftime('%Y-%m-%d')}: {len(session)} logs, "
                if session.is_complete_fc:
                    line += "complete"
                else:
                    # missing = map(ENCOUNTER_NAMES.__getitem__, session.missing_logs)
                    # line += "missing " + ", ".join(missing)
                    line += f"missing {len(session.missing_logs)} success logs"
                output[dt.year].append(line)

            for year in sorted(output):
                await ctx.send(f'Logs with associated tag "{tag}" in the year {year}:\n' + '\n'.join(output[year]))
        else:
            date += datetime.timedelta(hours=8)
            n = week_number(date)
            session = self.log_cache.filter_by_week(tag, n)
            output = []
            for log in session.logs:
                if log.is_success:
                    output.append(":white_check_mark: " + log.url)
                else:
                    output.append(":x: " + log.url)
            await ctx.send(f'Logs with associated tag "{tag}" for the week starting at '
                           f'{week_start_datetime(n).isoformat(sep=" ").split("+", 1)[0]} UTC:\n' + '\n'.join(output))

    @commands.command(hidden=True)
    async def unload_logs(self, ctx: commands.Context):
        msg = await ctx.send(f"Unloading all logs in the cache...")
        self.log_cache.unload_logs()
        await msg.edit(content=f"Unload complete.")

    @commands.command(hidden=True)
    async def num_loaded_logs(self, ctx: commands.Context):
        msg = await ctx.send(f"Counting all the loaded logs in the cache...")
        await msg.edit(content=f"There are {self.log_cache.num_loaded} loaded logs.")

    @commands.command()
    async def get_logs_by_encounter(self, ctx: commands.Context, tag: str, *, eid: Union[int, str] = ""):
        """
        List all logs associated with the given tag for the specified encounter

        :param tag: alphanumeric tag assigned to the logs
        :param eid: ID number of the encounter
        :return:
        """
        if tag not in self.log_cache.tags:
            await ctx.send(f"First argument must be a valid tag in use.  Available tags are:\n"
                           f"{', '.join(self.log_cache.tags)}")
            return

        links = [log.url for log in self.log_cache.filter_by_encounter(tag, eid).logs]
        links.insert(0, f'Logs with associated tag "{tag}" for the encounter {ENCOUNTERS[eid].name}:')
        for line in join_max_size('\n', links):
            await ctx.send(line)


def setup(bot):
    bot.add_cog(LogManager(bot))


def teardown(bot):
    pass
