#!/bin/python
import logging
import traceback
from typing import Optional

import discord
from discord.ext import commands
from discord.ext.commands import bot

from config import cfg, Config
from util.ei.log_cache import LogCache

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s:%(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

initial_extensions = ["cogs.stats_updater",
                      "cogs.admin",
                      # "cogs.auto_tagger",
                      "cogs.log_manager",
                      "cogs.arcdps",
                      #"cogs.api",
                      ]


class DiscordBot(bot.Bot):

    def __init__(self, *args, **kwargs):
        self.config = cfg
        self.stdout_channel: Optional[discord.TextChannel] = None
        self.log_cache = LogCache(self.config["log_cache"])
        super().__init__(*args, command_prefix='#!', **kwargs)
        # intents = discord.Intents.default()
        # super().__init__(*args, command_prefix='#!', intents=intents, **kwargs)

    async def on_ready(self):
        for extension in initial_extensions:
            try:
                self.load_extension(extension)
            except commands.ExtensionAlreadyLoaded:
                pass
            except Exception as e:
                tb = e.__traceback__
                stack_str = ''.join(traceback.StackSummary.from_list(traceback.extract_tb(tb)).format())
                logger.error("Assertion failed\n%s%s" % (stack_str, str(e)))

        if self.config["stdout_channel"]:
            self.stdout_channel = await self.fetch_channel(self.config["stdout_channel"])

    def run(self, *args, **kwargs):
        try:
            super(DiscordBot, self).run(self.config["discord_token"])
        finally:
            pass


if __name__ == "__main__":
    DiscordBot().run()
