import datetime
import json

from discord.ext import commands


class DateTimeEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {'__type__': 'datetime.datetime', 'value': obj.isoformat()}
        else:
            return super().default(obj)


class DateTimeDecoder(json.JSONDecoder):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, object_hook=self.object_hook,
                         **kwargs)

    def object_hook(self, d):
        if '__type__' not in d:
            return d
        if d['__type__'] == 'datetime.datetime':
            return datetime.datetime.fromisoformat(d['value'])


class DatetimeConverter(commands.Converter):
    async def convert(self, ctx: commands.Context, argument: str):
        for fmt in ("%Y-%m-%d",):
            try:
                return datetime.datetime.strptime(argument, fmt)
            except ValueError:
                pass
        raise commands.BadArgument