from util.ei.log import EliteInsightLog


class TwistedCastleLog(EliteInsightLog):

    ARENA_IMG = "https://i.imgur.com/txFtRNN.png"
    BUTTON_PXL = [(601, 366), (690, 822), (401, 693), (156, 842), (607, 517)]
    PXL_TO_CR = 0.75
    # druid button, left button, center button, back button, triple button

    def button_time(self, idx):
        length = len(self.data["crData"]["actors"][0]["positions"]) // 2
        num_players = self.num_players
        button_x, button_y = self.BUTTON_PXL[idx][0] * self.PXL_TO_CR, self.BUTTON_PXL[idx][1] * self.PXL_TO_CR
        for i in range(length):
            for p in range(num_players):
                x, y = self.data["crData"]["actors"][p*2]["positions"][2*i: 2*i+2]
                dx, dy = abs(button_x - x), abs(button_y - y)
                if dx < 6 and dy < 6:
                    return i * self.polling_rate / 1000
        return length * self.polling_rate / 1000