from util.ei.log import EliteInsightLog
from util.ei.util import get_obj_by_attr


class KeepConstructLog(EliteInsightLog):

    DETERMINED = 762
    XERAS_BOON = 35025
    MAGIC_BLAST = 35119
    UNBREAKABLE = 34979

    def determine_tank(self):
        pass

    def orb_collection_time(self, phase):
        kc = self.get_target("Keep Construct")
        times = get_obj_by_attr(kc["details"]["boonGraph"][0], "id", self.XERAS_BOON)["states"]
        if phase == 1:
            return times[1][0], times[2][0]
        else:
            return times[3][0], times[4][0]

    def breakbars(self):
        breakbars = self.breakbars_by_buffs("Keep Construct", self.UNBREAKABLE, 0, self.DETERMINED, self.XERAS_BOON)
        # check = self.breakbars_by_phase("Keep Construct")
        # for i in range(len(breakbars)):
        #     bb = breakbars[i]
        #     for cbb in check:
        #         if abs(bb[0] - cbb[0] - 2) < 1 and abs(bb[1] - cbb[1]) < 1:
        #             break
        #     else:
        #         # log may not have correctly simulated unbreakable
        #         pass
        return breakbars

    def breakbar_time(self, phase):
        phase = self.get_phase(f"Phase {phase}")
        for (t0, t1, broken) in self.breakbars():
            if phase["start"] <= t0 <= phase["end"]:
                return t0 - phase["start"]
        return None

    def breakbar_duration(self, phase):
        phase = self.get_phase(f"Phase {phase}")
        duration = 0
        for (t0, t1, broken) in self.breakbars():
            if broken and phase["start"] <= t0 <= phase["end"]:
                duration += t1 - t0
        return duration if duration else None

    def kc_health_at_breakbar(self, phase):
        phase = self.get_phase(f"Phase {phase}")
        # assumes breakbars are in order
        for (t0, t1, broken) in self.breakbars():
            if phase["start"] <= t0 <= phase["end"]:
                return self.target_health_perc("Keep Construct", t0)
        return self.target_health_perc("Keep Construct", phase["end"])

    def duration_if_breakbar_skipped(self, phase):
        phase_obj = self.get_phase(f"Phase {phase}")
        t = self.breakbar_time(phase)
        health = self.kc_health_at_breakbar(phase)
        phase_health_start = self.target_health_perc("Keep Construct", phase_obj["start"])
        rate = (phase_health_start - health) / t
        return 33.33 / rate

    def breakbar_ignored(self, phase):
        return self.breakbar_duration(phase) is None

    def num_breakbars_ignored(self):
        return sum(self.breakbar_duration(phase) is None for phase in range(1, 4))

    def avg_statue_lifetimes(self):
        duration = count = 0
        for a in self.data["crData"]["actors"]:
            if a["type"] == "Mob" and a["img"] == "https://i.imgur.com/qeYT1Bf.png":
                duration += a["end"] - a["start"]
                count += 1
        return duration / (1000 * count)

    def magic_blast(self):
        p2_i = self.get_phase_idx("Phase 2")
        p3_i = self.get_phase_idx("Phase 3")
        target = self.get_target("Keep Construct")
        p2_magic_blast_graph = get_obj_by_attr(target["details"]["boonGraph"][p2_i], "id", 35119)
        p3_magic_blast_graph = get_obj_by_attr(target["details"]["boonGraph"][p3_i], "id", 35119)
        return int(p2_magic_blast_graph["states"][0][1]), int(p3_magic_blast_graph["states"][0][1])