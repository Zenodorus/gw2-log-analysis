from util.ei.log import EliteInsightLog


class DhuumLog(EliteInsightLog):

    ARCING_AFFLICTION = 47646
    RESIDUAL_AFFLICTION = 47476

    def arcing_affliction(self):
        afflictions = []
        for p in self.pc_players:
            afflictions += [(v, p) for v in self.player_boon_times(p, self.ARCING_AFFLICTION)]
        afflictions.sort()

        data = []
        for a in afflictions:
            t = a[0][0]
            healths = [(self.player_health_perc(p, t), p) for p in self.pc_players]
            healths.sort()
            healths3 = [(self.player_health_perc(p, t - 3), p) for p in self.pc_players]
            healths3.sort()
            data.append((a, healths, healths3))

        data = []
        for i in range(1, len(afflictions)):
            a = afflictions[i]
            t = afflictions[i][0][0]
            p1 = afflictions[i-1][1]
            dists = [(self.player_distance(p1, p2, t), p2) for p2 in self.pc_players]
            dists.sort()
            data.append((a, dists))
        print(data)