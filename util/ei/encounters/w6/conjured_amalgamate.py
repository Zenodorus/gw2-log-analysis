from util.ei.log import EliteInsightLog
from util.ei.util import get_all_obj_by_attr, state_preimage, get_obj_by_attr


class ConjuredAmalgamateLog(EliteInsightLog):

    DETERMINED = 895
    CA_INVUL = 52255
    FRACTURED = 53030
    AUGMENTED_POWER = 52074

    def arm_phase(self, phase):
        phases = (get_all_obj_by_attr(self.data["phases"], "name", "Both Arms Phase") +
                  get_all_obj_by_attr(self.data["phases"], "name", "Left Arm Phase") +
                  get_all_obj_by_attr(self.data["phases"], "name", "Right Arm Phase"))
        phases.sort(key=lambda p: p["start"])
        return phases[phase - 1]

    def arm_phase_idx(self, phase):
        for i in range(len(self.data["phases"])):
            if self.phase_has_target("Left Arm", i) or self.phase_has_target("Right Arm", i):
                phase -= 1
            if phase == 0:
                return i
        raise IndexError

    def arm_health_end(self, phase, arm):
        idx = self.arm_phase_idx(phase)
        health = self.target_health_perc(target=arm, time=self.data["phases"][idx]["end"], phase=idx)
        return health

    def burn_phase(self, phase):
        phases = get_all_obj_by_attr(self.data["phases"], "name", "Burn Phase")
        # assumes phases are in order
        return phases[phase - 1]

    def burn_phase_idx(self, phase):
        for i in range(len(self.data["phases"])):
            if self.data["phases"][i]["name"] == "Burn Phase":
                phase -= 1
            if phase == 0:
                return i
        raise IndexError

    def avg_fractured(self, burn_phase, target):
        burn_phase = self.burn_phase(burn_phase)
        start, end = burn_phase["start"], burn_phase["end"]
        return self.avg_buff_stacks_by_time(target, False, self.FRACTURED, start, end)

    def time_to_10fractured(self, burn_phase):
        target = self.get_target("Conjured Amalgamate")
        boon_graph = target["details"]["boonGraph"][self.burn_phase_idx(burn_phase)]
        try:
            intervals = state_preimage(get_obj_by_attr(boon_graph, "id", self.FRACTURED)["states"], 10)
        except KeyError:
            intervals = []
        if intervals:
            return intervals[0][0]
        else:
            return None