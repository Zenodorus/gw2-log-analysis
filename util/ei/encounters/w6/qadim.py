from util.ei.log import EliteInsightLog


class QadimLog(EliteInsightLog):

    LAMP_IMG = 'https://i.imgur.com/89Kjv0N.png'

    def lamp_time(self, phase):
        all_lamps = [obj for obj in self.data["crData"]["actors"] if obj.get("img", "") == self.LAMP_IMG]
        if len(all_lamps) != 3:
            return None
        lamp = all_lamps[phase - 1]
        assert lamp["positions"][:2] == [409.16, 433.19] or lamp["positions"][:2] == [409.156, 433.194]
        return (lamp["end"] - lamp["start"]) / 1000

    def phase_time(self, phase: int):
        phase = self.get_phase(f"Qadim P{phase}")
        return phase["start"], phase["end"]