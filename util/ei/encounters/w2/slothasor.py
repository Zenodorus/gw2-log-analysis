from util.ei.log import EliteInsightLog


class SlothasorLog(EliteInsightLog):

    NACROLEPSY = 34467

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mechanics = []

    def narcolepsy_phase(self, phase: int):
        times = self.target_boon_times("Slothasor", self.NACROLEPSY, 1)
        try:
            start = 0 if phase == 1 else times[phase - 2][1]
            narcolepsy = self.duration if phase == len(times) + 1 else times[phase - 1][0]
            end = self.duration if phase == len(times) + 1 else times[phase - 1][1]
        except IndexError:
            return 0., 0., 0.
        else:
            return end - start, end - narcolepsy, self.target_health_perc("Slothasor", narcolepsy)
