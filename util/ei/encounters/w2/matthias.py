from util.ei.log import EliteInsightLog


class MatthiasLog(EliteInsightLog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mechanics = {}

    def _compute_mechanics(self):
        assert self._data is not None

        # idx = get_obj_idx_by_attr(self.data["mechanicMap"], key="name", value="Well")
        # for i in range(len(self.num_players)):
        # self.graphdata["mechanics"][idx]["points"][0]

    @property
    def data(self) -> dict:
        if self._data is None:
            self._data = super(MatthiasLog, self).data
            self._compute_mechanics()
        return self._data