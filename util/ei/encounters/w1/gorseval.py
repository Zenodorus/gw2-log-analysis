from util.ei.log import EliteInsightLog
from util.ei.util import get_obj_by_attr, state_changes


class GorsevalLog(EliteInsightLog):

    PROTECTIVE_SHADOW = 31877
    VIVID_ECHO = 31548

    def breakbar_duration(self, phase):
        phase = self.get_phase(f"Phase {phase}")

        for (t0, t1) in self.target_boon_times("Gorseval the Multifarious", self.VIVID_ECHO, 1):
            if phase["start"] <= t0 < phase["end"]:
                return t1 - t0
        return None

    def split_time(self, phase: int):
        gors = self.get_target('Gorseval the Multifarious')
        shadow_states = get_obj_by_attr(gors["details"]["boonGraph"][0], "id", self.PROTECTIVE_SHADOW)["states"]
        durations = state_changes(shadow_states, 1, 0)
        return durations[phase - 1][1] - durations[phase - 1][0]