from util.ei.log import EliteInsightLog


class SabirLog(EliteInsightLog):

    TARGET_NAME = "Cardinal Sabir"
    REPULSION_FIELD = 56172

    def breakbars(self):
        return [(s, e, True) for (s, e) in self.target_boon_times(self.TARGET_NAME, self.REPULSION_FIELD, 1)]

    def breakbar_duration(self, phase):
        phase = self.get_phase(f"Phase {phase}")
        duration = 0
        for (t0, t1, broken) in self.breakbars():
            if broken and phase["start"] <= t0 <= phase["end"]:
                duration += t1 - t0
        return duration if duration else None

    def sabir_health_at_shockwave(self):
        phase = self.get_phase(f"Phase 2")
        return self.target_health_perc("Cardinal Sabir", phase["start"] + 11)