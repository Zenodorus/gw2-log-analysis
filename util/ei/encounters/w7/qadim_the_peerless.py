from util.ei.log import EliteInsightLog


class QadimThePeerlessLog(EliteInsightLog):

    EAT_PYLON = 56446
    RUINOUS_NOVA_CHARGE = 56296
    FORCE_OF_RETALIATION_CAST = 56405

    ERRATIC_ENERGY = 56582
    KINETIC_ABUNDANCE = 56609

    def phase_time(self, phase):
        start = self.target_cast_times("Qadim the Peerless", self.FORCE_OF_RETALIATION_CAST)[phase - 1][0]
        if phase < 3:
            for skill_cast in self.target_cast_times("Qadim the Peerless", self.RUINOUS_NOVA_CHARGE):
                if start < skill_cast[0]:
                    return start, skill_cast[0]
        elif phase < 6:
            return start, self.target_cast_times("Qadim the Peerless", self.EAT_PYLON)[phase - 3][0]
        else:
            return start, self.data["phases"][0]["end"]

    def erratic_energy(self, phase):
        start, end = self.phase_time(phase)
        return self.avg_buff_stacks_by_time("Qadim the Peerless", False, self.ERRATIC_ENERGY, start, end)

    def kinetic_abundance_uptime(self, phase):
        start, end = self.phase_time(phase)
        uptime = count = 0
        for player in self.data["players"]:
            if player["group"] < 51:
                try:
                    uptime += self.avg_buff_stacks_by_time(player["acc"], True, self.KINETIC_ABUNDANCE, start, end)
                except KeyError:
                    # player did not ever receive kinetic abundance
                    pass
                count += 1
        return uptime * 100 / count