import datetime
import json
from typing import List, Dict, Union, Optional, Set, Iterable

from util import DateTimeDecoder, DateTimeEncoder
from util.ei.enum.encounter import ENCOUNTERS
from util.ei.log import EliteInsightLog, build_log
from util.ei.log_collection import SessionLogCollection, EncounterLogCollection, ENCOUNTER_CLS
from util.ei.util import week_number, week_start_datetime


class LogCacheError(Exception):
    pass


class LogConflict(LogCacheError):

    def __init__(self, new_log: EliteInsightLog, existing_log: EliteInsightLog, *args):
        self.new_log: EliteInsightLog = new_log
        self.existing_log: EliteInsightLog = existing_log
        super().__init__(*args)


class LogCache(object):

    # session_list, filter_by_week, filter by encounter
    # get, set, del

    def __init__(self, filename: str):
        self._filename = filename
        with open(filename, 'rb') as _f:
            json_cache = json.loads(_f.read().decode("utf-8-sig"), cls=DateTimeDecoder)
        if isinstance(json_cache, dict):
            json_cache = list(json_cache.values())

        self._cache: Dict[str, EliteInsightLog] = {}
        for metadata in json_cache:
            link = metadata["DpsReportEIUpload"]["Url"]
            if link is None:
                raise LogCacheError

            self._cache[link] = build_log(link, metadata)
            # in case metadata changes were done, unload
            self._cache[link].unload()

        self._cache_by_week: Dict[int, Set[str]] = {}
        self._cache_by_tag: Dict[str, Set[str]] = {}
        self._cache_by_enc: Dict[int, Set[str]] = {}
        self._sync_sub_caches()

    def save(self):
        with open(self._filename, 'w') as f:
            json.dump([log.metadata for log in self._cache.values()], f, cls=DateTimeEncoder, indent=2)
        self._sync_sub_caches()

    def _sync_sub_caches(self):
        self._cache_by_week = {}
        self._cache_by_tag = {}
        self._cache_by_enc = {}
        for link, log in self._cache.items():
            n = log.week_number
            if n not in self._cache_by_week:
                self._cache_by_week[n] = set()
            self._cache_by_week[n].add(link)

            for tag in log.tags:
                if tag["Name"] not in self._cache_by_tag:
                    self._cache_by_tag[tag["Name"]] = set()
                self._cache_by_tag[tag["Name"]].add(link)

            eid = log.encounter_id
            if eid not in self._cache_by_enc:
                self._cache_by_enc[eid] = set()
            self._cache_by_enc[eid].add(link)

    def add_logs(self, links: Union[str, Iterable[str]], tag: str):
        tag = {"Name": tag}
        if isinstance(links, str):
            links = [links]
        for link in links:
            if link in self._cache:
                log = self._cache[link]
                log.add_tag(tag)
                if not self._cache[link].is_cached:
                    self._cache[link] = EliteInsightLog(link, metadata=log.metadata, do_cache=True)
            else:
                # TODO dont use general log
                log = build_log(link, do_cache=True)
                for other_link in self._cache_by_week.get(log.week_number, []):
                    if log.is_same_fight(self._cache[other_link]):
                        raise LogConflict(log, self._cache[other_link])
                log.add_tag(tag)

    def get_logs(self, links: Union[str, Iterable[str]]):
        if isinstance(links, str):
            return self._cache[links]
        return [self._cache[link] for link in links]

    def remove_log(self, link):
        self._cache[link].delete_cache()
        del self._cache[link]
        self._sync_sub_caches()

    def unload_logs(self):
        for log in self._cache.values():
            log.unload()

    @property
    def tags(self):
        return tuple(self._cache_by_tag)

    @property
    def num_loaded(self):
        return sum(log.is_loaded for log in self._cache.values())

    def __getitem__(self, item):
        return self._cache[item]

    def __delitem__(self, key):
        del self._cache[key]

    def __contains__(self, item):
        return item in self._cache

    # subcollections

    def filter_by_week(self, tag: str, arg: Union[datetime.datetime, int] = 0) -> SessionLogCollection:
        if not arg:
            arg = week_number(datetime.datetime.utcnow())
        else:
            arg = arg if isinstance(arg, int) else week_number(arg)
        return SessionLogCollection(self.get_logs(self._cache_by_week[arg] & self._cache_by_tag[tag]))

    def filter_by_encounter(self, tag: str, enc: Union[str, int], result=None):
        enc = ENCOUNTERS[enc]
        cls = EncounterLogCollection if isinstance(ENCOUNTER_CLS[enc.wing_id], str) else ENCOUNTER_CLS[enc.wing_id]
        logs = self.get_logs(self._cache_by_tag[tag] & self._cache_by_enc[enc.wing_id])
        if result is not None:
            return cls(list(filter(lambda log: log.is_success, logs)))
        else:
            return cls(list(logs))

    def session_list(self, tag: str, start: Optional[Union[datetime.datetime, int]] = None,
                     end: Optional[Union[datetime.datetime, int]] = None,
                     min_build: int = 0, max_build: int = 0xFFFFFFFF) -> List[int]:
        logs = self.get_logs(self._cache_by_tag[tag])
        if start is not None:
            start = week_start_datetime(start) if isinstance(start, int) else start
            logs = filter(lambda log: start < log.end_time, logs)
        if end is not None:
            end = week_start_datetime(end) if isinstance(end, int) else end
            logs = filter(lambda log: log.end_time < end, logs)
        logs = filter(lambda log: min_build <= log.game_build <= max_build, logs)
        return sorted(list({log.week_number for log in logs}))
