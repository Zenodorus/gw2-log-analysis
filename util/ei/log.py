import datetime
import gzip
import math
import os
import json
import re
import logging
from typing import List, Union, Tuple, Optional

import bs4
import discord
import requests

from config import cfg
from util.ei import professions
from util.ei.enum import LEGEND_BOON_IDS
from util.ei.enum.encounter import ENCOUNTERS
from util.ei.enum.specialization import PROFESSIONS
from util.ei.util import get_obj_by_attr, week_number, eval_state, integrate_state, state_preimage_at_least, \
    state_preimage, interval_intersection, hms

logger = logging.getLogger("__main__")


class ObjectNotFound(Exception):
    pass


# boon ids

MIGHT = 740
FURY = 725
QUICKNESS = 1187
ALACRITY = 30328


LOGDATA_DIR = os.path.join(cfg["data_dir"], "logdata")
GRAPHDATA_DIR = os.path.join(cfg["data_dir"], "graphdata")

RE_DPS_REPORT = re.compile(r"https?://dps[.]report/(\w{4}-\d{8}-\d{6}_\w+)")
RE_DISCORD_ATTACH = re.compile(r"https?://cdn[.]discordapp[.]com/attachments/\d+/\d+/(\d{8}-\d{6}_\w+).html")

LOG_SUCCESS = 0
LOG_FAIL = 1


class EliteInsightLog(object):

    def __init__(self, link: str, metadata: Optional[dict] = None, do_cache: bool = False, data: Optional[dict] = None,
                 graphdata: Optional[dict] = None):
        if m := RE_DPS_REPORT.match(link):
            pass
        elif m := RE_DISCORD_ATTACH.match(link):
            pass
        else:
            raise Exception
        filename = f"{m.group(1)}.json.gz"

        keys = {"Encounter", "EncounterMode", "EncounterResult", "EncounterStartTime", "EncounterDuration",
                "DpsReportEIUpload", "FileName", "PointOfView", "Tags", "DataCached", "GameBuild", "Players"}
        if metadata is None:
            self.metadata = {
                "Encounter": None,
                "EncounterMode": None,
                "EncounterResult": None,
                "EncounterStartTime": None,
                "EncounterDuration": None,
                "DpsReportEIUpload": {
                     "Url": None,
                     "ProcessingError": None,
                     "UploadTime": None
                },
                "FileName": None,
                "PointOfView": {
                    "CharacterName": None,
                    "AccountName": None},
                "DataCached": False,
                "Tags": [],
                "GameBuild": None,
                "Players": [],
                }
        else:
            self.metadata = metadata.copy()
            assert keys >= set(metadata)
            assert self.metadata["DpsReportEIUpload"]["Url"] == link and self.metadata["FileName"] == filename

        self._data: Optional[dict] = data
        self._graphdata: Optional[dict] = graphdata
        self._players: Optional[dict] = None

        if do_cache or metadata is None:
            rawlogdata, rawgraphdata = self._download_dps_report(link)
            if rawlogdata is not None:
                self.metadata["FileName"] = filename
                self._data = json.loads(rawlogdata)
                self._graphdata = json.loads(rawgraphdata) if rawgraphdata != "{}" else self._data["graphData"]
                if do_cache:
                    self.save_to_cache()

        if metadata is None or keys > set(metadata):
            enc = ENCOUNTERS[self.data["fightID"]]
            if self.data["fightName"] not in ENCOUNTERS:
                logger.warn(f"Log found with unknown name {self.data['fightName']} that has a known fight ID "
                            f"{enc.target_id} for {enc.name}")
            else:
                assert ENCOUNTERS[self.data["fightID"]].wing_id == ENCOUNTERS[self.data["fightName"]].wing_id

            self.metadata["Encounter"] = enc.wing_id
            self.metadata["EncounterMode"] = 2 if self.data["fightName"].endswith("CM") else 1
            self.metadata["EncounterResult"] = int(not self.data["success"])
            self.metadata["EncounterStartTime"] = datetime.datetime.fromisoformat(self.data["encounterStart"])
            self.metadata["EncounterDuration"] = self.data["phases"][0]["duration"] / 1000
            pov = self.get_player(self._data["recordedBy"])
            self.metadata["PointOfView"] = {"CharacterName": pov["name"], "AccountName": f':{pov["acc"]}'}
            self.metadata["DpsReportEIUpload"] = {"Url": link, "ProcessingError": None, "UploadTime": None}
            self.metadata["FileName"] = filename
            self.metadata["GameBuild"] = self.data["gw2Build"]
            self.metadata["Players"] = [{"Name": p["name"], "AccountName": p["acc"]}
                                        for p in self.data["players"] if p["group"] < 51]


    @property
    def filename(self):
        return self.metadata["FileName"]

    @property
    def url(self):
        return self.metadata["DpsReportEIUpload"]["Url"]

    @property
    def is_cached(self):
        return self.metadata["DataCached"]

    @staticmethod
    def _download_dps_report(link):
        logdata = graphdata = None
        r = requests.get(link)
        if r.status_code == 200:
            bs = bs4.BeautifulSoup(r.text, features="lxml")
            logdata = graphdata = "{}"
            for tag in bs.find_all(name="script"):
                if isinstance(tag.string, str) and (tag.string.lstrip().startswith("var logData") or
                                                    tag.string.lstrip().startswith("var _logData")):
                    logdata = tag.string.split('=', 1)[1].split('\n')[0].lstrip().rstrip()[:-1]
                elif isinstance(tag.string, str) and (tag.string.lstrip().startswith("var graphData") or
                                                      tag.string.lstrip().startswith("var _graphData")):
                    graphdata = tag.string.split('=', 1)[1].split('\n')[0].lstrip().rstrip()[:-1]
        return logdata, graphdata

    def unload(self):
        self._data = None
        self._graphdata = None
        self._players = None

    @property
    def is_loaded(self) -> bool:
        return not(self._data is None and self._graphdata is None and self._players is None)

    @property
    def data(self) -> dict:
        if self._data is None:
            if self.metadata["DataCached"]:
                with gzip.open(os.path.join(LOGDATA_DIR, self.filename), 'rt', encoding='UTF-8') as _f:
                    self._data = json.load(_f)
            else:
                logdata, graphdata = self._download_dps_report(self.url)
                self._data = json.loads(logdata)
                self._graphdata = json.loads(graphdata)
        return self._data

    @property
    def graphdata(self) -> dict:
        if self._graphdata is None:
            if self.metadata["DataCached"]:
                with gzip.open(os.path.join(GRAPHDATA_DIR, self.filename), 'rt', encoding='UTF-8') as _f:
                    self._graphdata = json.load(_f)
                if not self._graphdata:
                    self._graphdata = self.data["graphData"]
            else:
                logdata, graphdata = self._download_dps_report(self.url)
                self._data = json.loads(logdata)
                self._graphdata = json.loads(graphdata)
        return self._graphdata

    @property
    def players(self) -> dict[Union[str, int], professions.Player]:
        if self._players is None:
            self._players = {}
            for p in self.data["players"]:
                try:
                    p = professions.PROFESSION_CLS[PROFESSIONS[p["profession"]].base](self, p)
                except KeyError:
                    p = professions.Player(self, p)
                self._players[p.idx] = p
                self._players[p.name] = p
                self._players[p.account] = p
        return self._players

    def save_to_cache(self):
        with gzip.open(os.path.join(LOGDATA_DIR, self.filename), 'wt', encoding='UTF-8') as zipfile:
            json.dump(self.data, zipfile)
        with gzip.open(os.path.join(GRAPHDATA_DIR, self.filename), 'wt', encoding='UTF-8') as zipfile:
            json.dump(self.graphdata, zipfile)
        self.metadata["DataCached"] = True

    def delete_cache(self):
        for d in (LOGDATA_DIR, GRAPHDATA_DIR):
            try:
                os.remove(os.path.join(d, self.filename))
            except FileNotFoundError:
                pass

    # metadata properties

    @property
    def start_time(self) -> datetime.datetime:
        return self.metadata["EncounterStartTime"]

    @property
    def strf_date(self) -> str:
        return self.start_time.astimezone(tz=None).strftime("%Y/%m/%d")

    @property
    def duration(self) -> float:
        return self.metadata["EncounterDuration"]

    @property
    def end_time(self) -> datetime.datetime:
        return self.metadata["EncounterStartTime"] + datetime.timedelta(seconds=self.duration)

    @property
    def encounter_id(self) -> int:
        return self.metadata["Encounter"]

    @property
    def encounter_icon(self) -> str:
        return f"https://dps.report{url}" if (url := self.data["fightIcon"]).startswith("/cache") else url

    @property
    def is_success(self) -> bool:
        return self.metadata["EncounterResult"] == 0

    @property
    def week_number(self) -> int:
        return week_number(self.end_time)

    @property
    def game_build(self):
        return self.metadata["GameBuild"]

    @property
    def tags(self):
        return self.metadata["Tags"]

    def add_tag(self, tag: Union[dict, str]):
        if isinstance(tag, dict):
            assert "Name" in tag and len(tag) == 1
            if tag not in self.metadata["Tags"]:
                self.metadata["Tags"].append(tag)
        elif isinstance(tag, str):
            tag = {"Name": tag}
            if tag not in self.metadata["Tags"]:
                self.metadata["Tags"].append(tag)

    def remove_tag(self, tag: Union[dict, str]):
        if isinstance(tag, str):
            tag = {"Name": tag}
        self.metadata["Tags"].remove(tag)

    def has_tag(self, tag: Union[dict, str]):
        if isinstance(tag, str):
            tag = {"Name": tag}
        return tag in self.metadata["Tags"]

    @property
    def profession_emojis(self):
        rv = []
        for s in self.subgroups:
            if s["id"] > 50:
                continue
            rv.append(''.join(p.emoji for p in sorted([self.players[p["name"]] for p in s["players"]])))
        return ' /'.join(rv)

    # combat data getters

    def get_phase(self, phase: Union[str, int]):
        if isinstance(phase, str):
            return get_obj_by_attr(self.data["phases"], "name", phase)
        elif isinstance(phase, int):
            return self.data["phases"][phase]

    def get_phase_idx(self, phase):
        for i in range(len(self.data["phases"])):
            if self.data["phases"][i]["name"] == phase:
                return i
        raise KeyError

    def get_obj_by_phase(self, phase: str, obj: list):
        for i in range(len(self.data["phases"])):
            if self.data["phases"][i]["name"] == phase:
                return obj[i]
        raise KeyError

    def get_target(self, name: Union[str, int]):
        if isinstance(name, str):
            return get_obj_by_attr(self.data["targets"], "name", name)
        else:
            return self.data["targets"][name]

    def get_obj_idx_by_target_phase(self, target: str, phase: int):
        targets = self.data["phases"][phase]["targets"]
        for i in range(len(targets)):
            if self.data["targets"][targets[i]]["name"] == target:
                return i
        raise KeyError

    def get_obj_by_target(self, obj, target):
        for i in range(len(self.data["targets"])):
            if self.data["targets"][i]["name"] == target:
                return obj[i]
        raise KeyError

    def get_player(self, name):
        if re.match(r"[a-zA-Z ]+.\d\d\d\d", name):
            return get_obj_by_attr(self.data["players"], "acc", name)
        else:
            return get_obj_by_attr(self.data["players"], "name", name)

    def get_obj_by_player(self, obj, player):
        for i in range(len(self.data["players"])):
            if self.data["players"][i]["acc"] == player:
                return obj[i]
        raise KeyError

    # combat timestamps

    def phase_time(self, phase: int):
        phase = self.get_phase(f"Phase {phase}")
        return phase["start"], phase["end"]

    def phase_duration(self, phase):
        start, end = self.phase_time(phase)
        return end - start

    # @property
    # def players(self):
    #     return tuple(map(lambda p: p["acc"], self.data["players"]))

    @property
    def pc_players(self):
        return tuple(map(lambda p: p["AccountName"], self.metadata["Players"]))

    @property
    def num_players(self):
        return len(self.metadata["Players"])

    @property
    def subgroups(self):
        ids = set(p["group"] for p in self.data["players"])
        subgroups = {i: [] for i in ids}
        for p in self.data["players"]:
            subgroups[p["group"]].append(p)
        return [{"id": k, "players": v} for k, v in subgroups.items()]

    @property
    def downs(self):
        return tuple(map(lambda p: p[8], self.data["phases"][0]["defStats"]))

    @property
    def deaths(self):
        return tuple(map(lambda p: p[10] > 0, self.data["phases"][0]["defStats"][:10]))

    def resurrect_time(self, phase: Union[str, int] = 0):
        return sum(player[7] for player in self.get_phase(phase)["supportStats"])

    def down_state_time(self, phase: Union[str, int] = 0):
        dst = 0
        for player in self.get_phase(phase)["defStats"]:
            if m := re.match(r"([\d.]*) seconds downed", player[9]):
                dst += float(m.group(1))
        return dst

    @property
    def cc_time(self):
        count = duration = 0
        for phase in self.data["phases"]:
            if phase["breakbarPhase"]:
                count += 1
                duration += phase["end"] - phase["start"] - 2
        return duration if count else None

    @property
    def polling_rate(self):
        return self.data["crData"]["pollingRate"] if self.data["crData"]["pollingRate"] else 150

    # player stats

    def player_health_perc(self, player: str, time: float) -> float:
        return eval_state(self.get_obj_by_player(self.graphdata["phases"][0]["players"], player)["healthStates"], time)

    def player_health(self, player: str, time: float) -> int:
        return int(self.player_health_perc(player, time) * PROFESSIONS[self.players[player].profession].health / 100)

    def player_dps(self, player, phase: Union[int, str] = 0):
        phase = self.get_phase(phase)
        # TODO ["dpsStatsTargets"][0][target_id]
        return round(self.get_obj_by_player(phase["dpsStatsTargets"], player)[0][0] / phase["duration"] * 1000)

    def player_peak_dps(self, player, phase: Union[int, str] = 0):
        phase = self.get_phase_idx(phase) if isinstance(phase, str) else phase
        graph = self.get_obj_by_player(self.graphdata["phases"][phase]["players"], player)
        dmg = graph["damage"]["targets"][0]
        return round(max(dmg[i] / i for i in range(1, len(dmg))))

    def player_incoming_damage(self, player, phase: Union[int, str] = 0):
        return self.get_obj_by_player(self.get_phase(phase)["defStats"], player)[0]

    def player_average_health_percentage(self, player, phase: Union[int, str] = 0):
        phase = self.get_phase_idx(phase) if isinstance(phase, str) else phase
        graph = self.get_obj_by_player(self.graphdata["phases"][phase]["players"], player)
        start, end = graph["healthStates"][0][0], graph["healthStates"][-1][0]
        return integrate_state(graph["healthStates"], start, end) / (end - start)

    def player_died(self, player):
        return self.get_obj_by_player(self.data["phases"][0]["defStats"], player)[10] > 0

    def player_downs(self, player):
        return self.get_obj_by_player(self.data["phases"][0]["defStats"], player)[8]

    def player_downstate_time(self, player):
        player = self.get_obj_by_player(self.data["phases"][0]["defStats"], player)
        if m := re.match(r"([\d.]*) seconds downed", player[9]):
            return float(m.group(1))
        return 0

    def player_distance(self, player1, player2, time):
        x1, y1 = self.players[player1].position(time)
        x2, y2 = self.players[player2].position(time)
        dx, dy = x1 - x2, y1 - y2
        return math.sqrt(dx*dx + dy*dy)

    # target stats

    def target_health_perc(self, target: str, time: float, phase: int = 0) -> float:
        idx = self.get_obj_idx_by_target_phase(target, phase)
        states = self.graphdata["phases"][phase]["targets"][idx]["healthStates"]
        return eval_state(states, time)

    def phase_has_target(self, target: str, phase: int):
        for i in self.data["phases"][phase]["targets"]:
            if self.data["targets"][i]["name"] == target:
                return True
        return False

    # generic combat info

    def boon_times(self, boon_id, value):
        times = []
        for player in self.data["players"]:
            boon_graph = player["details"]["boonGraph"][0]
            try:
                times.append(state_preimage_at_least(get_obj_by_attr(boon_graph, "id", boon_id), value))
            except KeyError:
                times.append([])

    def target_boon_times(self, target, boon_id, value):
        target = self.get_target(target)
        states = get_obj_by_attr(target["details"]["boonGraph"][0], "id", boon_id)["states"]
        return state_preimage(states, value)

    def time_to_25mfqa(self, phase: Union[str, int]):
        if isinstance(phase, str):
            phase_idx = self.get_phase_idx(phase)
        else:
            phase_idx = phase

        times = []
        for player in self.data["players"]:
            if player["group"] > 50:
                continue
            boon_graph = player["details"]["boonGraph"][phase_idx]
            try:
                intervals = interval_intersection(
                    state_preimage(get_obj_by_attr(boon_graph, "id", MIGHT)["states"], 25),
                    state_preimage(get_obj_by_attr(boon_graph, "id", FURY)["states"], 1),
                    state_preimage(get_obj_by_attr(boon_graph, "id", QUICKNESS)["states"], 1),
                    state_preimage(get_obj_by_attr(boon_graph, "id", ALACRITY)["states"], 1))
            except KeyError:
                intervals = []
            if intervals:
                times.append(intervals[0][0])
            else:
                times.append(self.data["phases"][phase_idx]["end"])
        return sum(times) / len(times)

    def uptime_25mfqa(self, phase: Union[str, int]):
        if isinstance(phase, str):
            phase_idx = self.get_phase_idx(phase)
        else:
            phase_idx = phase

        durations = []
        for player in self.data["players"]:
            if player["group"] > 50:
                continue
            boon_graph = player["details"]["boonGraph"][phase_idx]
            try:
                intervals = interval_intersection(
                    state_preimage(get_obj_by_attr(boon_graph, "id", MIGHT)["states"], 25),
                    state_preimage(get_obj_by_attr(boon_graph, "id", FURY)["states"], 1),
                    state_preimage(get_obj_by_attr(boon_graph, "id", QUICKNESS)["states"], 1),
                    state_preimage(get_obj_by_attr(boon_graph, "id", ALACRITY)["states"], 1))
            except KeyError:
                intervals = []
            durations.append(sum(i[1] - i[0] for i in intervals) * 100000 / self.data["phases"][phase_idx]["duration"])
        return sum(durations) / len(durations)

    def player_boon_times(self, player: str, boon_id: int, value=1) -> List[Tuple[float, float]]:
        """
        Get the time intervals for which the player has the specified boon at the specified value
        :param player: account or character name
        :param boon_id: id number of the boon
        :param value: number of stacks of the boon
        :return: 
        """
        player = self.get_player(player)
        try:
            states = get_obj_by_attr(player["details"]["boonGraph"][0], "id", boon_id)["states"]
        except KeyError:
            return []
        else:
            return state_preimage(states, value)

    def breakbars_by_buffs(self, target, trigger_buff, trigger_value, buff_if_broken, buff_if_not_broken):
        target = self.get_target(target)
        boon_graph = target["details"]["boonGraph"][0]
        bb_triggers: List[Tuple[float, Optional[bool]]]
        if trigger_value == 0:
            bb_triggers = [(s[0], None) for s in get_obj_by_attr(boon_graph, "id", trigger_buff)["states"] if s[1] == 0]
        elif trigger_value == 1:
            bb_triggers = [(s[0], None) for s in get_obj_by_attr(boon_graph, "id", trigger_buff)["states"] if s[1] > 0]
        else:
            bb_triggers = [(s[0], None) for s in get_obj_by_attr(boon_graph, "id", trigger_buff)["states"]
                           if s[1] > trigger_value]

        try:
            is_broken = [(s[0], True) for s in get_obj_by_attr(boon_graph, "id", buff_if_broken)["states"] if s[1] > 0]
        except KeyError:
            is_broken = []
        try:
            is_not_broken = [(s[0], False) for s in get_obj_by_attr(boon_graph, "id", buff_if_not_broken)["states"]
                             if s[1] > 0]
        except KeyError:
            is_not_broken = []

        states = bb_triggers + is_broken + is_not_broken
        states.sort()
        breakbars = []
        for i in range(1, len(states)):
            if states[i-1][1] is None and states[i][1] is not None and states[i][1]:
                breakbars.append((states[i-1][0], states[i][0], True))
            if states[i-1][1] is None and states[i][1] is not None and not states[i][1]:
                breakbars.append((states[i-1][0], states[i][0], False))
            if states[i-1][1] is None and states[i][1] is None and i == len(states) - 1:
                breakbars.append((states[i-1][0], states[i][0], False))
        return breakbars

    def breakbars_by_phase(self, target):
        return [(phase["start"], phase["end"]) for phase in self.data["phases"]
                if phase["name"].startswith(f"{target} Breakbar")]

    def target_cast_times(self, target, skill_id):
        target = self.get_target(target)
        rotation = target["details"]["rotation"][0]
        return [skill for skill in rotation if skill[1] == skill_id]

    def ttl_buff_stacks_by_time(self, name, is_player, buff_id, start, end):
        actor = self.get_player(name) if is_player else self.get_target(name)
        try:
            obj = get_obj_by_attr(actor["details"]["boonGraph"][0], "id", buff_id)
        except KeyError:
            return 0
        return integrate_state(obj["states"], start, end)

    def avg_buff_stacks_by_time(self, name, is_player, buff_id, start, end):
        return self.ttl_buff_stacks_by_time(name, is_player, buff_id, start, end) / (end - start)

    def movement(self):
        movement = 0
        target = None
        for actor in self.data["crData"]["actors"]:
            if actor["type"] == "Target":
                target = actor
                break
        if target is None:
            return None
        x, y = target["positions"][0], target["positions"][1]
        for i in range(1, len(target["positions"]) // 2):
            x2, y2 = target["positions"][i * 2], target["positions"][i * 2 + 1]
            movement += math.sqrt((x - x2) ** 2 + (y - y2) ** 2)
            x, y = x2, y2
        return movement

    def moving(self):
        moving = 0
        target = None
        for actor in self.data["crData"]["actors"]:
            if actor["type"] == "Target":
                target = actor
                break
        if target is None:
            return None
        x, y = target["positions"][0], target["positions"][1]
        for i in range(1, len(target["positions"]) // 2):
            x2, y2 = target["positions"][i * 2], target["positions"][i * 2 + 1]
            if x != x2 or y != y2:
                moving += 1
            x, y = x2, y2
        return moving * self.polling_rate / 1000

    def is_same_fight(self, other: "EliteInsightLog"):
        if self.encounter_id != other.encounter_id:
            return False
        if self == other:
            return True
        if set(self.pc_players) != set(other.pc_players):
            return False
        start = max(self.start_time, other.start_time)
        end = min(self.end_time, other.end_time)
        if end < start:
            return False
        duration = (end - start).total_seconds()
        if duration <= 0.9 * min(self.duration, other.duration):
            return False
        return True

    def __lt__(self, other: "EliteInsightLog"):
        # note this is intentionally in reverse
        return self.start_time > other.start_time

    def __eq__(self, other: "EliteInsightLog"):
        return (isinstance(other, EliteInsightLog) and
                self.metadata["PointOfView"]["AccountName"] == other.metadata["PointOfView"]["AccountName"] and
                self.encounter_id == other.encounter_id and self.start_time == other.start_time)

    def __repr__(self):
        return f"<Log Encounter={self.encounter_id} StartTime={self.start_time} Url={self.url}>"

    def determine_tank(self):
        return

    @property
    def embed(self):
        embed = discord.Embed(title=ENCOUNTERS[self.encounter_id].name,
                              description="Elements are boldface if they are inferred.\n"
                                          "Builds are italicized if requirements were met to match the build.  Use"
                                          "#!build [name] for more details.",
                              timestamp=datetime.datetime.utcnow())
        if self.metadata["EncounterMode"] == 2:
            embed.title += " CM"
        embed.set_thumbnail(url=self.encounter_icon)
        embed.add_field(name="Report", value=f'[{self.strf_date}]({self.url})', inline=True)
        embed.add_field(name="Duration", value=hms(self.duration), inline=True)
        embed.add_field(name="Result", inline=True,
                        value="Success :white_check_mark:" if self.is_success else "Failure :x:")
        for subgroup in self.subgroups:
            if subgroup["id"] > 50:
                continue
            # embed.add_field(name=f"__Subgroup {subgroup['id']}__", inline=False,
            #                 value='\n\n'.join(self.player_role(p["name"]) for p in subgroup["players"]))
            # embed.add_field(name=f"__Subgroup {subgroup['id']}__", value="", inline=False)
            first = True
            for p in subgroup["players"]:
                p = self.players[p["name"]]
                embed.add_field(name=(f"__Subgroup {subgroup['id']}__\n" if first else "") + f"{p.emoji} {p.name}\n",
                                value=p.role_embed_field(), inline=False)
                first = False
            # embed.add_field(name=f"__Subgroup {subgroup['id']}__", inline=False,
            #                 value='\n\n'.join(self.players[p["name"]].role_embed_field() for p in subgroup["players"]))

        return embed

    def player_role(self, name):
        player = self.get_player(name)
        phase = self.get_phase(0)
        try:
            output = f"{self.players[name].emoji} {name}\n"
        except KeyError:
            output = f"• {name}\n"

        dps_type = self.players[name].damage_type
        role = []
        traits = self.players[name].traits
        skills = []
        legends = []
        with_attrs = []
        if player['profession'] == "Mirage":
            if player["l1Set"] == player["l2Set"] == ["Staff"]:
                if dps_type:
                    role.append("DPS")
                with_attrs.append("Staff")
            elif (len(player["l1Set"]) == len(player["l2Set"]) == 2 and
                  player["l1Set"][0] == player["l2Set"][0] == "Axe" and
                  {player["l1Set"][1], player["l2Set"][1]} == {"Pistol", "Torch"}):
                if dps_type:
                    role.append("DPS")
                with_attrs.append("Axe")

        if self.players[name].base_profession == "Revenant":
            for l_name, l_id in LEGEND_BOON_IDS.items():
                if self.player_boon_times(name, l_id, 1):
                    legends.append(l_name)
        else:
            skills = [s.name for s in self.players[name].skills]

        if with_attrs:
            with_attrs = ' with ' + ', '.join(with_attrs)
        else:
            with_attrs = ""

        if self.players[name].exact_build:
            output += f"- Build: *{self.players[name].exact_build['name']}*"
        elif dps_type:
            output += f"- Build: {dps_type} {player['profession']}{with_attrs}"
        else:
            output += f"- Build: {player['profession']}{with_attrs}"

        if role:
            output += f"\n- Roles: {', '.join(role)}"
        # if traits:
        #     output += f"\n- Traits: {', '.join(traits)}"
        if skills:
            output += f"\n- Skills: {', '.join(skills)}"
        if legends:
            output += f"\n- Legends: {', '.join(legends)}"
        return output


def build_log(link, metadata=None, do_cache=False):
    import util.ei.encounters as encounters

    log_cls = {11: encounters.ValeGuardianLog,
               13: encounters.GorsevalLog,
               14: 'Sabetha the Saboteur',

               21: encounters.SlothasorLog,
               22: 'Bandit Trio',
               23: encounters.MatthiasLog,

               32: encounters.KeepConstructLog,
               33: encounters.TwistedCastleLog,
               34: 'Xera',

               41: 'Cairn the Indomitable',
               42: 'Mursaat Overseer',
               43: 'Samarog',
               44: 'Deimos',

               51: 'Soulless Horror',
               52: 'Desmina',
               53: 'Broken King',
               54: 'Eater of Souls',
               55: 'Eye of Judgment',
               56: encounters.DhuumLog,

               61: encounters.ConjuredAmalgamateLog,
               62: 'Nikare',
               63: encounters.QadimLog,

               71: 'Cardinal Adina',
               72: encounters.SabirLog,
               73: encounters.QadimThePeerlessLog}

    if metadata is None:
        log = EliteInsightLog(link=link, metadata=None, do_cache=do_cache)
        if isinstance(log_cls[log.encounter_id], str):
            return log
        return log_cls[log.encounter_id](link=link, metadata=log.metadata, do_cache=do_cache,
                                         data=log.data, graphdata=log.graphdata)
    else:
        if isinstance(log_cls[metadata["Encounter"]], str):
            return EliteInsightLog(link, metadata, do_cache=do_cache)
        return log_cls[metadata["Encounter"]](link, metadata, do_cache=do_cache)
