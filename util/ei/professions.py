from typing import Union, Optional, List, TYPE_CHECKING

from util.ei.enum import Skill, SKILLS, DUAL_SKILLS, NOT_SLOTTED_SKILLS, PASSIVE_SIGNET_IDS
from util.ei.enum.specialization import Build, PROFESSIONS, BUILDS
from util.ei.util import get_obj_by_attr, get_array_by_idx, integrate_state

if TYPE_CHECKING:
    from orly.logstats import EliteInsightLog


class Player(object):

    def __init__(self, log: "EliteInsightLog", player: Union[str, int, dict]):
        self._log = log
        if isinstance(player, dict):
            for i, p in enumerate(self._log.data["players"]):
                if p is player:
                    self._data = player
                    self._idx = i
        else:
            for i, p in enumerate(self._log.data["players"]):
                if p["acc"] == player:
                    self._data = p
                    self._idx = i
                elif p["name"] == player:
                    self._data = p
                    self._idx = i
                elif i == player:
                    self._data = p
                    self._idx = i

        # self._traits: Optional[List[str]] = None
        # self._skills: Optional[List[Skill]] = None
        if self.profession not in ("NPC", "Sword"):
            self.build: Build = Build(self.profession)
            self._exact_build: Optional[dict] = None

            self._determine_traits()
            self._determine_skills()
            self.build.damage = self.damage_type
            for k in ("l1Set", "l2Set"):
                if self._data[k]:
                    self.build.add_weapon(*self._data[k])
        else:
            self.build = None
            self._exact_build: Optional[dict] = None

    @property
    def account(self) -> str:
        return self._data["acc"]

    @property
    def name(self) -> str:
        return self._data["name"]

    @property
    def idx(self) -> int:
        return self._idx

    @property
    def unique_id(self) -> int:
        return self._data["uniqueID"]

    @property
    def base_profession(self):
        return PROFESSIONS[self.profession].base

    @property
    def profession(self) -> str:
        return self._data["profession"]

    @property
    def emoji(self) -> str:
        if self.account in ("Tyuu.3790", "Kalculations.8294"):
            return '🍉'
        return PROFESSIONS[self.profession].emoji

    @property
    def subgroup(self) -> int:
        return self._data["group"]

    def rotation(self, break_at_weapon_swap: bool = False, phase: int = 0):
        # rotation [start time, skill id, duration, skill status, ???]
        # skill status: basic=1, interrupted=2, full aftercast=3, instant=4
        if break_at_weapon_swap:
            skills = self._data["details"]["rotation"][phase]
            rotation = []
            row = []
            for s in skills:
                if s[1] != -2:
                    row.append(s)
                else:
                    rotation.append(row)
                    row = []
            if row:
                rotation.append(row)
            return rotation
        else:
            return self._data["details"]["rotation"][phase]

    def min_recharge(self, skill_id, ammo=1):
        uses = [s for s in self.rotation() if s[1] == skill_id and s[3] != 2]
        if len(uses) <= ammo:
            return self._log.duration
        return min(uses[i + ammo][0] - uses[i][0] for i in range(len(uses) - ammo))

    # Generic Combat

    @property
    def resurrection_time(self):
        return self._log.data["phases"][0]["supportStats"][self._idx][7]

    def position(self, time):
        actor = get_obj_by_attr(self._log.data["crData"]["actors"], "id", self.unique_id)
        idx = int(time / self._log.polling_rate * 1000) * 2
        return actor["positions"][idx:idx+2]

    @property
    def damage_type(self) -> str:
        # TODO get the key for all targets
        for dps_stats in ("dpsStatsTargets",):
            stats = [0, 0, 0, 0]
            for target in self._log.get_phase(0)[dps_stats][self._idx]:
                for i in range(4):
                    stats[i] += target[i]
            total, power, condi, cc = stats
            if power > 1.5 * condi:
                return "Power"
            elif condi > 1.5 * power:
                return "Condition"
            elif power == condi == 0:
                pass
            else:
                return "Hybrid"
        return ""

    # Traits

    def _determine_traits(self):
        if self.profession != self.base_profession:
            self.build.add_specialization(self.profession)

    @property
    def traits(self) -> list:
        return self.build.traits

    # Skills

    def _determine_skills(self):
        for s in self.skills:
            self.build.add_skill(s)

    @property
    def skill_list(self) -> list:
        return self._data["details"]["dmgDistributions"][0]["distribution"]

    def has_skill(self, skill: Union[Skill, int]):
        try:
            get_array_by_idx(self.skill_list, 1, skill if isinstance(skill, int) else skill.id)
        except KeyError:
            return False
        else:
            return True

    def get_skill(self, skill: Union[Skill, int]):
        try:
            return get_array_by_idx(self.skill_list, 1, skill if isinstance(skill, int) else skill.id)
        except KeyError:
            return None

    @property
    def heal_skill(self) -> Optional[Skill]:
        for s in self.skill_list:
            skill = SKILLS.get(s[1], None)
            if skill and skill.profession == self.base_profession and skill.type == "Heal":
                return skill

    @property
    def utility_skills(self) -> List[Skill]:
        skills = []
        for s in self.skill_list:
            skill = SKILLS.get(s[1], None)
            if skill and skill.profession == self.base_profession and skill.type == "Utility":
                if skill.id in DUAL_SKILLS:
                    skill = SKILLS[DUAL_SKILLS[skill.id]]
                if skill not in skills and skill.id not in NOT_SLOTTED_SKILLS:
                    skills.append(skill)
        if len(skills) < 3:
            for bid, sid in PASSIVE_SIGNET_IDS.items():
                skill = SKILLS[sid]
                if self.has_boon(bid) and skill.profession == self.base_profession and skill.type == "Utility":
                    if skill not in skills:
                        skills.append(skill)
        if len(skills) > 3:
            print(skills)
        return skills

    @property
    def elite_skill(self) -> Optional[Skill]:
        for s in self.skill_list:
            skill = SKILLS.get(s[1], None)
            if skill and skill.profession == self.base_profession and skill.type == "Elite":
                return skill
        for bid, sid in PASSIVE_SIGNET_IDS.items():
            skill = SKILLS[sid]
            if self.has_boon(bid) and skill.profession == self.base_profession and skill.type == "Elite":
                return skill

    @property
    def skills(self) -> List[Skill]:
        return list(filter(None, [self.heal_skill] + self.utility_skills + [self.elite_skill]))

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError

    # Builds

    @property
    def exact_build(self) -> Optional[dict]:
        if self._exact_build is None:
            for b in BUILDS:
                if b["required"] < self.build:
                    self._exact_build = b
                    return b
        return self._exact_build

    # Boons

    @property
    def boon_graph(self) -> list:
        return self._data["details"]["boonGraph"][0]

    def has_boon(self, b_id: int) -> bool:
        try:
            get_obj_by_attr(self.boon_graph, "id", b_id)
        except KeyError:
            return False
        else:
            return True

    def get_boon(self, b_id: int) -> Optional[dict]:
        try:
            return get_obj_by_attr(self.boon_graph, "id", b_id)
        except KeyError:
            return None

    # Other

    def role_embed_field(self) -> str:
        output = f"{self.emoji} {self.name}\n"
        output = ""
        if self.exact_build:
            output += f"Build: *{self.exact_build['name']}*\nRole: {self.exact_build['role']}"
            build = self.build - self.exact_build["build"]
        elif self.damage_type:
            output += f"Build: {self.damage_type} {self.profession}"
            build = self.build
        else:
            output += f"Build: {self.profession}"
            build = self.build

        traits = tuple(filter(None, (t for line in build.traits for t in line["major"])))
        if traits:
            output += f"\nTraits: {', '.join(t.name for t in traits)}"

        skills = tuple(filter(None, build.skills))
        if skills:
            output += f"\nSkills: {', '.join(s.name for s in skills)}"
            # if self.exact_build:
            #     num_common = len(tuple(s for s in self.build.skills[1:4] if s in self.exact_build["build"].skills[1:4]
            #                            and s is not None))
            #     num_none = len(tuple(s for s in self.build.skills[1:4] if s is None))
            #     num_mismatch = 3 - num_common - num_none
            #
            #     skills = tuple(filter(None, (self.exact_build["build"] - self.build).skills[1:4]))
            #     if skills and num_none == 0:
            #         output += f" (replace {', '.join(s.name for s in skills)})"

        weapons = tuple(filter(None, build.weapons))
        if weapons:
            weapons = ' / '.join('/'.join(map(str, ws)) for ws in weapons)
            output += f"\nWeapon Set: {weapons}"
        return output

    def def_stats(self, phase: Union[str, int] = 0) -> list:
        return self._log.get_phase(phase)["defStats"][self._idx]

    def __lt__(self, other):
        if isinstance(other, Player):
            if self.subgroup > other.subgroup:
                return False
            return PROFESSIONS[self.profession].id < PROFESSIONS[other.profession].id


class Warrior(Player):

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_boon(42539):
            self.build.add_trait("Berserker's Power")

        if self.has_boon(30204):
            self.build.add_trait("Furious")

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Guardian(Player):

    SIGNET_OF_RESOLVE_BUFF = -1000
    SIGNET_OF_RESOLVE_SKILL = 9158

    TOME_SKILLS = [41258, 40635, 42449, 40015, 42898,  # Tome of Justice
                   45022, 40679, 45128, 42008, 42925,  # Tome of Resolve
                   42986, 41968, 41836, 40988, 44455]  # Tome of Courage

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_skill(21795):
            self.build.add_trait("Glacial Heart")

        if self.has_skill(13677):
            self.build.add_specialization("Zeal")

        if self.has_boon(56890):
            self.build.add_trait("Symbolic Avenger")

        if self.has_boon(30207):
            self.build.add_trait("Invigorated Bulwark")

        if self.profession == "Firebrand":
            # interrupted skills do not use up a page
            counts = []
            tome_duration = []
            count = last_swap = 0
            for s in self.rotation():
                if s[1] in Guardian.TOME_SKILLS and s[3] != 2:
                    count += 1
                elif s[1] == -2:
                    if count:
                        counts.append(count)
                        tome_duration.append(s[0] - last_swap)
                        count = 0
                    last_swap = s[0]

            # TODO this needs to be better
            if sum(counts) / len(counts) > 5.2:
                self.build.add_trait("Archivist of Whispers")

            if "deathRecap" in self._data["details"]:
                live_time = self._data["details"]["deathRecap"][0]["time"] / 1000
            else:
                live_time = self._log.duration

            # quickness sources: fmw (6s - gives double), potence (2.5s @ 12cd), swift scholar (3s @ 8icd)
            # liberator's vow + mantra of solace (2s @ 12cd)
            ally_skill_quick = 0
            if fmw := self.get_skill(29965):
                ally_skill_quick += fmw[5] * 3

            alac_factor = integrate_state(self.get_boon(30328)["states"]) / live_time * 0.25 + 1
            effective_cd = 12 / alac_factor
            max_count = int(live_time / effective_cd + 1)
            # mantra of potence
            if len(self.utility_skills) < 3:
                ally_skill_quick += max_count * 2.5

            # mantra of solace
            liberator_quick = 0
            if self.heal_skill is None:
                liberator_quick += max_count * 2

            swift_scholar = sum(t > 8 for t in tome_duration) * 3
            for sid in (44364, 41780, 42259):
                if s := self.get_skill(sid):
                    swift_scholar += s[5] * 3

            fb_rune = 1.4 if self.has_skill(9628) else 1

            try:
                idx = self._log.data["boons"].index(1187)
                self_quick = self._log.data["phases"][0]["boonGenSelfStats"][self._idx]["data"][idx]
                # TODO might be 1 instead of 0
                self_quick = (self_quick[0] + self_quick[2]) * self._log.duration / 100
            except ValueError:
                pass
            else:
                if (ally_skill_quick + liberator_quick + swift_scholar) * fb_rune < self_quick:
                    self.build.add_trait("Stalwart Speed")
                    if self.heal_skill is None:
                        self.build.add_skill(41714)
                    if len(self.utility_skills) < 3:
                        self.build.add_skill(40915)

    @property
    def heal_skill(self) -> Optional[Skill]:
        if hs := super().heal_skill:
            return hs

        if self.has_boon(Guardian.SIGNET_OF_RESOLVE_BUFF):
            return SKILLS[Guardian.SIGNET_OF_RESOLVE_SKILL]

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Revenant(Player):

    ASSASSINS_PRESENCE = 26854
    IMPROVED_KALLAS_FERVOR = 45614
    EXPOSE_DEFENSES = 48894
    BATTLE_SCARS = 26646

    LEGEND_SKILLS = (28494,  # Mallyx
                     28134,  # Shiro
                     28195,  # Ventari
                     28419,  # Jalis
                     28085,  # Glint
                     41858,  # Kalla
                     62891,  # Alliance
                     )

    def _determine_skills(self):
        for s in self.skill_list:
            if s[1] in Revenant.LEGEND_SKILLS:
                self.build.add_skill(SKILLS[s[1]])

    def _check_vindication(self) -> bool:
        targets = self._log.data["targets"]
        try:
            skill = get_array_by_idx(self._data["details"]["dmgDistributionsTargets"][0][0]["distribution"], 1, 42836)
        except KeyError:
            return False

        if len(targets) == 1 or targets[0]["name"] == "Dhuum":
            return skill[6] > 10 * skill[5]

        if len(self._data["details"]["dmgDistributionsTargets"]) == 1:
            distributions = self._data["details"]["dmgDistributionsTargets"]
        else:
            distributions = self._data["details"]["dmgDistributionsTargets"][1:]

        hits = 0
        for distribution in distributions:
            for target in distribution:
                try:
                    hits += get_array_by_idx(target["distribution"], 1, 42836)[6]
                except KeyError:
                    pass

        return hits > 10 * skill[5]

    def _determine_traits(self) -> None:
        super()._determine_traits()

        # this is not reliable.  deimos logs start well into combat
        if self.has_boon(Revenant.EXPOSE_DEFENSES) or self.has_boon(Revenant.BATTLE_SCARS):
            # Devastation traitline
            # traits.append("Expose Defenses")

            has_ap = False
            try:
                idx = self._log.data["offBuffs"].index(Revenant.ASSASSINS_PRESENCE)
            except ValueError:
                pass
            else:
                # TODO might be 0 instead of 1
                if self._log.data["phases"][0]["offBuffGenSelfStats"][self._idx]["data"][idx][1]:
                    self.build.add_trait("Assassin's Presence")
                    has_ap = True

            if not has_ap:
                if self.damage_type == "Condition":
                    # self.build.add_trait("**Thrill of Combat**")
                    self.build.add_trait("Thrill of Combat")
                elif self.damage_type == "Power":
                    # self.build.add_trait("**Notoriety**")
                    self.build.add_trait("Notoriety")

        if self.profession == "Renegade":
            if self.has_boon(Revenant.IMPROVED_KALLAS_FERVOR):
                self.build.add_trait("Lasting Legacy")
            elif self._check_vindication():
                self.build.add_trait("Vindication")
            else:
                # self.build.add_trait("**Righteous Rebel**")
                self.build.add_trait("Righteous Rebel")

    @property
    def signets(self) -> List[Skill]:
        return []


class Ranger(Player):

    SPOTTER = 14055

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_boon(14055):
            self.build.add_trait("Spotter")
        if self.has_boon(29703):
            self.build.add_trait("Quick Draw")
        if self.profession == "Druid":
            if self.has_boon(32248):
                self.build.add_trait("Lingering Light")
            else:
                self.build.add_trait("Grace of the Land")
                # traits.append("**Grace of the Land**")

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Engineer(Player):

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_boon(38333):
            self.build.add_trait("Pinpoint Distribution")
        if self.has_boon(59601):
            self.build.add_trait("Big Boomer")
        if self.has_boon(59528):
            self.build.add_trait("Explosive Temper")

    @property
    def signets(self) -> List[Skill]:
        return []


class Thief(Player):

    SIGNET_OF_MALICE = (13049, 13050)
    SOUL_STONE_VENOM = 49077

    def _determine_traits(self) -> None:
        super()._determine_traits()

        # Improvisation
        n_uses = 0
        for sid in (49063, 49112,):
            try:
                n_uses += get_array_by_idx(self.skill_list, 1, sid)[5]
            except KeyError:
                pass
        if int(self._log.duration / 20) + 2 < n_uses:
            self.build.add_trait("Improvisation")

        if self.has_skill(14136):
            self.build.add_trait("Uncatchable")

        # Daredevil Grandmaster Traits
        if self.profession == "Daredevil" and self.has_skill(65001):
            if self.has_boon(32200):
                self.build.add_trait("Lotus Training")
            elif self.has_boon(33162):
                self.build.add_trait("Bounding Dodger")
            # elif self.has_boon():
            #     traits.append("Unhindered Combatant")

    @property
    def heal_skill(self) -> Optional[Skill]:
        if hs := super().heal_skill:
            return hs

        if self.has_boon(Thief.SIGNET_OF_MALICE[0]):
            return SKILLS[Thief.SIGNET_OF_MALICE[1]]

    @property
    def utility_skills(self) -> List[Skill]:
        skills = []
        for s in self.skill_list:
            skill = SKILLS.get(s[1], None)
            if skill and skill.profession == self.base_profession and skill.type == "Utility":
                if skill.id == 16435:
                    skill = SKILLS[13038]
                elif skill.id == 30693:
                    skill = SKILLS[30868]
                elif skill.id == 13106:
                    skill = SKILLS[13002]
                if skill not in skills and skill.id not in NOT_SLOTTED_SKILLS:
                    skills.append(skill)
        if len(skills) < 3:
            for bid, sid in PASSIVE_SIGNET_IDS.items():
                skill = SKILLS[sid]
                if self.has_boon(bid) and skill.profession == self.base_profession and skill.type == "Utility":
                    if skill not in skills:
                        skills.append(skill)
        if len(skills) > 3:
            print(skills)
        return skills

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Necromancer(Player):

    HEAL_SIGNET = (21761, 21762)  # Signet of Vampirism

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.profession == "Scourge":
            if self.has_boon(43626):
                self.build.add_trait("Sadistic Searing")
            else:
                # traits.append("**Desert Empowerment**")
                self.build.add_trait("Desert Empowerment")

        if self.has_boon(53489):
            self.build.add_trait("Soul Barbs")

        # if self.has_skill(10544) and self.min_recharge(10544, ammo=2) < (30 / 1.25) * 2:
        #     self.build.add_trait("Master of Corruption")

        # if self.has_skill(45846) and self.min_recharge(45846) < (20 / 1.25):
        #     self.build.add_trait("Fell Beacon")

        # if self.has_skill(44296) and self.min_recharge(44296) < (25 / 1.25):
        #     self.build.add_trait("Fell Beacon")

    @property
    def heal_skill(self) -> Optional[Skill]:
        if self.has_boon(Necromancer.HEAL_SIGNET[0]):
            return SKILLS[Necromancer.HEAL_SIGNET[1]]

        for s in self.skill_list:
            # Taste of Death -> Summon Blood Fiend
            if s[1] == 10577:
                return SKILLS[10547]

            skill = SKILLS.get(s[1], None)
            if skill and skill.type == "Heal":
                return skill

    @property
    def elite_skill(self) -> Optional[Skill]:
        for s in self.skill_list:
            # Charge -> Summon Flesh Golem
            if s[1] == 10647:
                return SKILLS[10646]

            skill = SKILLS.get(s[1], None)
            if skill and skill.type == "Elite":
                return skill

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Mesmer(Player):

    # Signets (Buff, Skill)
    HEAL_SIGNET = (21751, 21750)        # Signet of the Ether
    UTILITY_SIGNETS = [(10231, 10232),  # Signet of Domination
                       (10235, 10236),  # Signet of Inspiration
                       (10233, 10234),  # Signet of Midnight
                       ]                # Signet of Illusions
    ELITE_SIGNET = (30739, 29519)       # Signet of Humility

    UNTRACKABLE_SKILLS = [10202,        # Mirror Images
                          10302,        # Feedback
                          10201,        # Decoy
                          10200,        # Blink
                          10237,        # Mantra of Concentration
                          10204,        # Mantra of Distraction
                          10207,        # Mantra of Resolve
                          45046,        # Illusionary Ambush (Mirage)
                          43064,        # Sand through Glass (Mirage)
                          ]

    def _check_chronophantasma(self):
        # TODO phases may be useful
        # array of (minion name, player skill id, minion skill id)
        minions = {"Illusionary Warden":       (10282, 22477),
                   "Illusionary Swordsman":    (10174, 49067),
                   "Illusionary Disenchanter": (10267, 10266),
                   "Illusionary Duelist":      (10175, 10284),
                   "Illusionary Mage":         (10189, 10166)}
        for idx in range(len(self._data["minions"])):
            # the + 1 is in case there is a cast before the log starts
            try:
                if self._data["minions"][idx]["name"] in minions:
                    psid, msid = minions[self._data["minions"][idx]["name"]]
                    p_skill = get_array_by_idx(self._data["details"]["dmgDistributions"][0]["distribution"], 1, psid)
                    m_skill = get_array_by_idx(
                        self._data["details"]["minions"][idx]["dmgDistributions"][0]["distribution"], 1, msid)
                    if p_skill[5] + 1 < m_skill[5]:
                        return True
            except KeyError:
                pass

        return False

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_boon(30426):
            self.build.add_trait("Fencer's Finesse")
        if self.has_boon(49058):
            self.build.add_trait("Compounding Power")
        if self.has_skill(13733):
            self.build.add_trait("Method of Madness")

        # need to look out for signet of the ether recharging the phantasm
        if (self.min_recharge(10310) < (10 / 1.25) or  # self.min_recharge(10216) < (18 / 1.25) or
                self.min_recharge(10331) < (25 / 1.25) or self.min_recharge(10169) < (35 / 1.25)):
            self.build.add_trait("Chaotic Potency")

        # # the -1 is for extra slosh because of cast times and when cooldown actually starts
        # if self.min_recharge(10186) < (25 / 1.25) - 1:
        #     self.build.add_trait("Warden's Feedback")
        # # need to look out for signet of the ether recharging the warden
        # elif self.heal_skill != SKILLS[21750] and self.min_recharge(10282) < (20 / 1.25):
        #     self.build.add_trait("Warden's Feedback")

        if self.min_recharge(10168) < (12 / 1.25):
            self.build.add_trait("Malicious Sorcery")

        if self.profession == "Chronomancer":
            if self._check_chronophantasma():
                self.build.add_trait("Chronophantasma")
            else:
                self.build.add_trait("Seize the Moment")
                # traits.append("**Seize the Moment**")

            if self.has_boon(10243):
                self.build.add_trait("Blurred Inscriptions")

        if self.profession == "Mirage":
            if self.has_skill(41324) or self.has_skill(40614):
                self.build.add_trait("Mirrored Axes")
            elif self.min_recharge(45243, ammo=2) < (8 / 1.25) * 2 or self.min_recharge(43761) < (10 / 1.25):
                self.build.add_trait("Mirrored Axes")

    @property
    def heal_skill(self) -> Optional[Skill]:
        if hs := super().heal_skill:
            return hs

        if self.has_boon(Mesmer.HEAL_SIGNET[0]):
            return SKILLS[Mesmer.HEAL_SIGNET[1]]

    @property
    def utility_skills(self) -> List[Skill]:
        skills = set()
        for bid, sid in Mesmer.UTILITY_SIGNETS:
            if self.has_boon(bid):
                skills.add(SKILLS[sid])

        for s in self.skill_list:
            skill = SKILLS.get(s[1], None)
            if skill and skill.profession == self.base_profession and skill.type == "Utility":
                # Skill 44677 is Mirage Mirror, which is not a slotted utility skill, but the mirror on the ground
                if skill not in skills and skill.id != 44677:
                    skills.add(skill)

        # Blood Shard skills from Matthias projectiles.  This means they were reflected.
        # TODO care must be taken for Medic's Feedback (Inspiration) and Warden's Feedback (Inspiration)
        if self.has_skill(34480) or self.has_skill(34440):
            # Mirror effect
            if self.has_boon(10357):
                pass
            # Compounding Power (Illusions) and Distortion.  Covers Master of Fragmentation.
            elif self.has_boon(49058) and self.has_boon(10243):
                pass
            # Magnetic Aura
            elif self.has_boon(5684):
                pass
            else:
                skills.add(SKILLS[10302])

        # self stability sources:  mantra (has group gen), inspiration signet (has buff and cast time),
        # precog well (has cast time), bountiful disillusionment (if chaos spec, self only),
        # sand shards (damaging mirage trait skill, self only)

        if len(skills) > 3:
            print(skills)
        return list(skills)

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


class Elementalist(Player):

    def _determine_traits(self) -> None:
        super()._determine_traits()

        if self.has_skill(21636):
            self.build.add_trait("Electric Discharge")

        # TODO check self gen
        if self.has_boon(5587):
            self.build.add_trait("Soothing Mist")

        if self.has_skill(56883):
            self.build.add_specialization("Fire")

        if self.has_boon(13342):
            self.build.add_trait("Persisting Flames")

    @property
    def signets(self) -> List[Skill]:
        raise NotImplementedError


PROFESSION_CLS = {
    "Warrior": Warrior,
    "Guardian": Guardian,
    "Revenant": Revenant,
    "Ranger": Ranger,
    "Engineer": Engineer,
    "Thief": Thief,
    "Necromancer": Necromancer,
    "Mesmer": Mesmer,
    "Elementalist": Elementalist,
}
