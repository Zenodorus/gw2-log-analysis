import datetime
from typing import List, Any, Dict, Tuple, Iterable

from util.ei.enum import RESET_TIMESTAMP


def week_start_datetime(n: int):
    if n > 0:
        return datetime.datetime.fromtimestamp((n - 1) * 3600 * 24 * 7 + RESET_TIMESTAMP, tz=datetime.timezone.utc)


def week_number(dt: datetime.datetime) -> int:
    t = dt.timestamp()
    q, r = divmod(t, 3600*24*7)
    if r > RESET_TIMESTAMP:
        q += 1
    return int(q)


def hms(seconds: float, pad_hours: bool = False) -> str:
    sign = '-' if seconds < 0 else ''
    m, s = divmod(abs(seconds), 60)
    h, m = divmod(int(m), 60)
    if pad_hours:
        return f"{sign}{h:02.0f}:{m:02.0f}:{s:06.3f}"
    else:
        return f"{sign}{h}:{m:02.0f}:{s:06.3f}" if h else f"{sign}{m}:{s:06.3f}"


# _ = object()
# def get_obj_by_attr(array: List[Dict[str, Any]], key: str, value: Any, default: Any = _):
def get_obj_by_attr(array: List[Dict[str, Any]], key: str, value: Any):
    # TODO this may be bad to have default value None
    for obj in array:
        if obj.get(key, None) == value:
            return obj
    raise KeyError
    # if default is _:
    #     raise KeyError
    # return default


def get_obj_idx_by_attr(array: List[Dict[str, Any]], key: str, value: Any):
    # TODO this may be bad to have default value None
    for idx in range(len(array)):
        if array[idx].get(key, None) == value:
            return idx
    raise KeyError



def get_array_by_idx(array: List[List[Any]], idx: int, value: Any):
    for subarray in array:
        if subarray[idx] == value:
            return subarray
    raise KeyError


def get_all_obj_by_attr(array: List[Dict[str, Any]], key: str, value: Any):
    # TODO this may be bad to have default value None
    results = []
    for obj in array:
        if obj.get(key, None) == value:
            results.append(obj)
    return results


def state_changes(xy_states: List[List[float]], y_1: float, y_2: float):
    return [(xy_states[i-1][0], xy_states[i][0]) for i in range(1, len(xy_states))
            if xy_states[i-1][1] == y_1 and xy_states[i][1] == y_2]


def eval_state(states: List[List[float]], x: float) -> float:
    for i in range(1, len(states)):
        if states[i-1][0] <= x < states[i][0]:
            return states[i-1][1]
    if x < min(states)[0]:
        return 0
    else:
        return max(states)[1]


def integrate_state(states, x0=None, x1=None):
    if x0 is None:
        x0 = states[0][0]
    if x1 is None:
        x1 = states[-1][0]

    value = 0
    x, y = x0, eval_state(states, x0)
    for i in range(1, len(states)):
        if x1 < states[i][0]:
            break
        elif x < states[i][0]:
            value += (states[i][0] - x) * y
            x, y = states[i]
    value += (x1 - x) * y
    return value


def state_preimage(states, y) -> List[Tuple[float, float]]:
    results = []
    for i in range(1, len(states)):
        if states[i-1][1] == y:
            results.append((states[i-1][0], states[i][0]))
    return results


def state_preimage_at_least(states, y):
    results = []
    for i in range(1, len(states)):
        if states[i-1][1] >= y:
            if results[-1][1] == states[i-1][0]:
                results[-1] = (results[-1][0], states[i][0])
            else:
                results.append((states[i-1][0], states[i][0]))
    return results


def interval_intersection(*union_sets):
    if len(union_sets) > 2:
        union_set_1 = interval_intersection(*union_sets[:-1])
        union_set_2 = union_sets[-1]
    else:
        union_set_1, union_set_2 = union_sets

    results = []
    for i1 in union_set_1:
        for i2 in union_set_2:
            j = (max(i1[0], i2[0]), min(i1[1], i2[1]))
            if j[0] < j[1]:
                results.append(j)
    return results


def positional_cmp(l1: list, l2: list):
    n1, n2 = len(l1), len(l2)
    for i in range(min(n1, n2)):
        if l1[i] is not None and l1[i] != l2[i]:
            return False
    if n1 > n2:
        for i in range(n2, n1):
            if l2[i] is not None:
                return False
    return True


def join_max_size(sep: str, iterable: Iterable, max_size: int = 2000):
    output = []
    current = ""
    for item in iterable:
        if len(item) >= max_size:
            raise ValueError

        if current:
            if len(current) + len(sep) + len(item) < max_size:
                current += sep + item
            else:
                output.append(current)
                current = item
        else:
            current = item

    if current:
        output.append(current)
    return output
