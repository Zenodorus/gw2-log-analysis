import datetime
from typing import List, Dict, Union, Tuple, Optional

import discord

from util.ei.enum.encounter import ENCOUNTER_NAMES
from util.ei.log import EliteInsightLog, LOG_SUCCESS, LOG_FAIL
from util.ei.encounters.w3.twisted_castle import TwistedCastleLog
from util.ei.encounters.w6.conjured_amalgamate import ConjuredAmalgamateLog
from util.ei.encounters.w1.gorseval import GorsevalLog
from util.ei.encounters.w2.slothasor import SlothasorLog
from util.ei.encounters.w3.keep_construct import KeepConstructLog
from util.ei.util import hms


class LogCollection(object):

    def __init__(self, logs: List[EliteInsightLog]):
        self.logs: List[EliteInsightLog] = logs
        self.logs.sort(key=lambda l: l.start_time)

    def subset(self, enc=None, start=None, end=None):
        cls = self.__class__
        if isinstance(enc, int):
            results = [log for log in self.logs if log.encounter_id == enc]
            cls = ENCOUNTER_CLS[enc]
        elif isinstance(enc, tuple) or isinstance(enc, list):
            results = [log for log in self.logs if log.encounter_id in enc]
        elif isinstance(enc, str):
            raise Exception("not implemented")
        else:
            results = self.logs.copy()

        if start is None:
            start = max(results).start_time
        if end is None:
            end = min(results).start_time
        return cls([log for log in results if start < log.start_time < end])

    def unload(self):
        for log in self.logs:
            log.unload()

    def sort(self, key, reverse=False, status=None) -> List[EliteInsightLog]:
        if status is None:
            results = [(key(log), log) for log in self.logs]
        elif status == LOG_SUCCESS:
            results = [(key(log), log) for log in self.logs if log.is_success]
        elif status == LOG_FAIL:
            results = [(key(log), log) for log in self.logs if not log.is_success]
        else:
            raise Exception

        results = [x for x in results if x[0] is not None]
        results.sort(key=lambda x: (-x[0], x[1]), reverse=reverse)
        return results

    def average(self, func, status=None):
        if status is None:
            results = tuple(func(log) for log in self.logs)
        elif status == LOG_SUCCESS:
            results = tuple(func(log) for log in self.logs if log.is_success)
        elif status == LOG_FAIL:
            results = tuple(func(log) for log in self.logs if not log.is_success)
        else:
            raise Exception

        results = tuple(x for x in results if x is not None)
        return sum(results) / len(results) if results else None

    def sum(self, func, status=None):
        if status is None:
            results = tuple(func(log) for log in self.logs)
        elif status == LOG_SUCCESS:
            results = tuple(func(log) for log in self.logs if log.is_success)
        elif status == LOG_FAIL:
            results = tuple(func(log) for log in self.logs if not log.is_success)
        else:
            raise Exception

        return sum(x for x in results if x is not None)

    @property
    def success_logs(self):
        return tuple(filter(lambda log: log.is_success, self.logs))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.unload()

    def __len__(self):
        return len(self.logs)


class SessionLogCollection(LogCollection):

    SPLIT_IDS = [13, "pre-sab", 14, "pre-vg", 11, "pre-kc", 32, "pre-tc", 33, "pre-xera", 34, "pre-sloth", 21,
                 "pre-trio", 22, "pre-matt", 23, "pre-cairn", 41, "pre-mo", 42, "pre-sam", 43, "pre-deimos", 44,
                 "pre-adina", 71, "pre-sabir", 72, "pre-qtp", 73, "pre-ca", 61, "pre-largos", 62, "pre-qadim", 63,
                 "pre-sh", 51, "pre-river", 52, "pre-statues", "statues", "pre-dhuum", 56]

    def __init__(self, logs: List[EliteInsightLog]):
        super().__init__(logs)
        self._success_logs: Dict[Union[str, int], EliteInsightLog] = {}
        self._encounter_order: List[int] = []
        self._split_times: Dict[Union[int, Tuple[int, int]], float] = {}

        start = end = None
        peid: Optional[int] = None
        for log in self.logs:
            eid = log.encounter_id
            if not self._encounter_order or self._encounter_order[-1] != eid:
                start = log.start_time
                self._encounter_order.append(eid)
            if peid is not None:
                self._split_times[peid, eid] = (start - end).total_seconds()
                peid = None

            if log.is_success:
                # if log.encounter_id in self._success_logs:
                #     raise Exception
                self._success_logs[log.encounter_id] = log
                self._success_logs[ENCOUNTER_NAMES[log.encounter_id]] = log
                end = log.end_time
                peid = eid
                self._split_times[eid] = (end - start).total_seconds()

    @property
    def date(self) -> datetime.datetime:
        return self.start_time + self.duration / 2

    @property
    def start_time(self) -> datetime.datetime:
        return self.logs[0].start_time

    @property
    def end_time(self) -> datetime.datetime:
        return self.logs[-1].end_time

    @property
    def duration(self) -> datetime.timedelta:
        return self.end_time - self.start_time

    @property
    def num_fail_logs(self) -> int:
        return sum(map(lambda log: not log.is_success, self.logs))

    @property
    def num_success_logs(self) -> int:
        return sum(map(lambda log: log.is_success, self.logs))

    @property
    def is_complete_fc(self):
        return len(self._success_logs) // 2 == 25

    @property
    def missing_logs(self):
        fc_ids = (11, 13, 14, 21, 22, 23, 32, 33, 34, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 61, 62, 63, 71, 72, 73)
        return tuple(i for i in fc_ids if i not in self._success_logs)

    @property
    def encounter_order(self):
        return self._encounter_order

    # statistics

    @property
    def split_times(self):
        return self._split_times

    @property
    def statistics(self):
        dst = self.down_state_time
        rr = self.resurrect_time / dst
        stats = {"failed combat time": self.failed_combat_time,
                 "num fail logs": self.num_fail_logs,
                 "duration": self.duration.total_seconds(),
                 "down state time": dst,
                 "resurrection rate": rr,
                 "deaths": self.deaths,
                 }
        return stats



    @property
    def successful_combat_time(self):
        return round(sum(log.duration for log in self.logs if log.is_success), 3)

    @property
    def failed_combat_time(self):
        return round(sum(log.duration for log in self.logs if not log.is_success), 3)

    @property
    def downtime(self):
        return round(self.duration.total_seconds() - self.successful_combat_time, 3)

    def encounter(self, eid):
        return self._success_logs[eid]

    def all_split_times(self):
        return tuple(self.encounter_time(enc) for enc in SessionLogCollection.SPLIT_IDS)

    @property
    def all_players(self) -> Dict[str, int]:
        players = {}
        for log in self.logs:
            for p in log.metadata["Players"]:
                if p["AccountName"] not in players:
                    players[p["AccountName"]] = 0
                players[p["AccountName"]] += 1
        return players

    def encounter_time(self, enc):
        if enc in self._success_logs:
            return self._success_logs[enc].duration
        elif enc == "pre-sab":
            return (self._success_logs[14].start_time - self._success_logs[13].end_time).total_seconds()
        elif enc == "pre-vg":
            return (self._success_logs[11].start_time - self._success_logs[14].end_time).total_seconds()
        elif enc == "pre-kc":
            return (self._success_logs[32].start_time - self._success_logs[11].end_time).total_seconds()
        elif enc == "pre-tc":
            return (self._success_logs[33].start_time - self._success_logs[32].end_time).total_seconds()
        elif enc == "pre-xera":
            return (self._success_logs[34].start_time - self._success_logs[33].end_time).total_seconds()
        elif enc == "pre-sloth":
            return (self._success_logs[21].start_time - self._success_logs[34].end_time).total_seconds()
        elif enc == "pre-trio":
            return (self._success_logs[22].start_time - self._success_logs[21].end_time).total_seconds()
        elif enc == "pre-matt":
            return (self._success_logs[23].start_time - self._success_logs[22].end_time).total_seconds()
        elif enc == "pre-cairn":
            return (self._success_logs[41].start_time - self._success_logs[23].end_time).total_seconds()
        elif enc == "pre-mo":
            return (self._success_logs[42].start_time - self._success_logs[41].end_time).total_seconds()
        elif enc == "pre-sam":
            return (self._success_logs[43].start_time - self._success_logs[42].end_time).total_seconds()
        elif enc == "pre-deimos":
            return (self._success_logs[44].start_time - self._success_logs[43].end_time).total_seconds()
        elif enc == "pre-adina":
            return (self._success_logs[71].start_time - self._success_logs[44].end_time).total_seconds()
        elif enc == "pre-sabir":
            return (self._success_logs[72].start_time - self._success_logs[71].end_time).total_seconds()
        elif enc == "pre-qtp":
            return (self._success_logs[73].start_time - self._success_logs[72].end_time).total_seconds()
        elif enc == "pre-ca":
            return (self._success_logs[61].start_time - self._success_logs[73].end_time).total_seconds()
        elif enc == "pre-largos":
            return (self._success_logs[62].start_time - self._success_logs[61].end_time).total_seconds()
        elif enc == "pre-qadim":
            return (self._success_logs[63].start_time - self._success_logs[62].end_time).total_seconds()
        elif enc == "pre-sh":
            return (self._success_logs[51].start_time - self._success_logs[63].end_time).total_seconds()
        elif enc == "pre-river":
            return (self._success_logs[52].start_time - self._success_logs[51].end_time).total_seconds()
        elif enc == "pre-statues":
            return (self._success_logs[53].start_time - self._success_logs[52].end_time).total_seconds()
        elif enc == "statues":
            return (self._success_logs[55].end_time - self._success_logs[53].start_time).total_seconds()
        elif enc == "pre-dhuum":
            return (self._success_logs[56].start_time - self._success_logs[55].end_time).total_seconds()

    def wing_time(self, wing):
        if wing == 1:
            return (self._success_logs[11].end_time - self.start_time).total_seconds()
        elif wing == 2:
            return (self._success_logs[23].end_time - self._success_logs[34].end_time).total_seconds()
        elif wing == 3:
            return (self._success_logs[34].end_time - self._success_logs[11].end_time).total_seconds()
        elif wing == 4:
            return (self._success_logs[44].end_time - self._success_logs[23].end_time).total_seconds()
        elif wing == 5:
            return (self._success_logs[56].end_time - self._success_logs[63].end_time).total_seconds()
        elif wing == 6:
            return (self._success_logs[63].end_time - self._success_logs[73].end_time).total_seconds()
        elif wing == 7:
            return (self._success_logs[73].end_time - self._success_logs[44].end_time).total_seconds()

    @property
    def deaths(self):
        deaths = 0
        for log in self.logs:
            if not log.is_success:
                deaths += len(log.pc_players)
            else:
                deaths += sum(log.deaths)

        self.unload()
        return deaths

    @property
    def down_state_time(self):
        return self.sum(func=lambda log: log.down_state_time(), status=LOG_SUCCESS)

    @property
    def resurrect_time(self):
        return self.sum(func=lambda log: log.resurrect_time(), status=LOG_SUCCESS)
        # duration = 0
        # for log in self.success_logs:
        #     for player in log.data["phases"][0]["supportStats"]:
        #         duration += player[7]
        # self.unload()
        # return duration


class EncounterLogCollection(LogCollection):

    @property
    def latest_log(self):
        return self.success_logs[-1]

    def recent_logs(self, a: int, b: int):
        return self.__class__(sorted(self.logs)[a:b])

    def best_time_log(self):
        return self.sort(key=lambda l: l.duration, status=LOG_SUCCESS)[0]

    def best_cc_time(self):
        return self.sort(key=lambda l: l.cc_time, status=LOG_SUCCESS)[0]


class ValeGuardianLogCollection(EncounterLogCollection):

    def report(self, other_log):
        output = ["Vale Gaurdian", " "]
        for log in self.logs:
            output.append(f"{log.strf_date}: {log.url}")
        output.append(f"**{other_log.strf_date}: {other_log.url}**")
        output.append(" ")

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        output.append(f'{"**" if is_better else ""}Full Fight Duration: '
                      f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')
        output.append(" ")

        for p in range(1, 4):
            if p > 2:
                stat = self.average(func=lambda l: l.split_time(1))
                other_stat = other_log.split_time(1)
                is_better = other_stat < stat
                output.append(f'{"**" if is_better else ""}Split 1 Duration: '
                              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                stat = self.average(func=lambda l: l.split_time(2))
                other_stat = other_log.split_time(2)
                is_better = other_stat < stat
                output.append(f'{"**" if is_better else ""}Split 2 Duration: '
                              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                stat = self.average(func=lambda l: l.breakbar_duration(p))
                other_stat = other_log.breakbar_duration(p)
                if other_stat is None:
                    output.append(f'{"**" if stat is not None else ""}Breakbar {p} Duration: '
                                  f'ignored{"**" if is_better else ""}')
                elif stat is None:
                    output.append(f'Breakbar {p} Duration: '
                                  f'{hms(other_stat)} (all recent logs ignored)')
                else:
                    is_better = other_stat < stat
                    output.append(f'{"**" if is_better else ""}Breakbar {p} Duration: '
                                  f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.get_phase(f"Phase {p}")["duration"] / 1000)
            other_stat = other_log.get_phase(f"Phase {p}")["duration"] / 1000
            is_better = other_stat < stat
            output.append(f'{"**" if is_better else ""}Phase {p} Duration: '
                          f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(f"Phase {p}"))
            other_stat = other_log.time_to_25mfqa(f"Phase {p}")
            is_better = other_stat < stat
            output.append(f'{"**" if is_better else ""}Phase {p} Time to 25MFQA: '
                          f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
            other_stat = other_log.uptime_25mfqa(f"Phase {p}")
            is_better = other_stat > stat
            output.append(f'{"**" if is_better else ""}Phase {p} 25MFQA Uptime: '
                          f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            output.append(" ")

        # stat = self.sort(key=lambda l: l.num_breakbars_ignored(), reverse=True)[0][0]
        # other_stat = other_log.num_breakbars_ignored()
        # is_better = other_stat > stat
        # output.append(f'{"**" if is_better else ""}Number of Breakbars Ignored: '
        #               f'{other_stat} (max {stat}){"**" if is_better else ""}')
        #
        # output.append(" ")
        #
        # stat = self.average(func=lambda l: l.avg_statue_lifetimes())
        # other_stat = other_log.avg_statue_lifetimes()
        # is_better = other_stat < stat
        # output.append(f'{"**" if is_better else ""}Avg Statue Lifetime: '
        #               f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')
        #
        # magic_blast = self.sort(key=lambda l: sum(l.magic_blast()), status=LOG_SUCCESS)[0][0]
        # other_magic_blast = sum(other_log.magic_blast())
        # is_better = (other_magic_blast < magic_blast)
        # output.append(f'{"**" if is_better else ""}Magic Blast (orbs): '
        #               f'{other_magic_blast} (min {magic_blast}){"**" if is_better else ""}')

        output.append(" ")
        output.append("Boldface indicates improvement over the 4 week average")
        return '\n'.join(output)


class GorsevalLogCollection(EncounterLogCollection):

    def report(self, other_log: GorsevalLog):
        embed = discord.Embed(title="Gorseval Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0x0000FF)

        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({other_stat - stat:+.3f})'
                              f'{"**" if is_better else ""}')

        for p in range(1, 4):
            value = []
            stat = self.average(func=lambda l: l.get_phase(f"Phase {p}")["duration"] / 1000)
            other_stat = other_log.get_phase(f"Phase {p}")["duration"] / 1000
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Duration: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(f"Phase {p}"))
            other_stat = other_log.time_to_25mfqa(f"Phase {p}")
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Time to 25MFQA: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
            other_stat = other_log.uptime_25mfqa(f"Phase {p}")
            is_better = other_stat > stat
            value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
                         f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            value.append(" ")

            stat = self.average(func=lambda l: l.breakbar_duration(p))
            other_stat = other_log.breakbar_duration(p)
            if other_stat is None:
                value.append(f'- {"**" if stat is not None else ""}Breakbar Duration: '
                             f'ignored{"**" if stat is not None else ""}')
            elif stat is None:
                value.append(f'- Breakbar Duration: '
                             f'{hms(other_stat)} (all recent logs ignored)')
            else:
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Breakbar Duration: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            embed.add_field(name=f"Phase {p}", value='\n'.join(value), inline=False)

        value = []
        stat = self.average(func=lambda l: l.split_time(1))
        other_stat = other_log.split_time(1)
        is_better = other_stat < stat
        value.append(f'- {"**" if is_better else ""}Split 1 Duration: '
                     f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

        stat = self.average(func=lambda l: l.split_time(2))
        other_stat = other_log.split_time(2)
        is_better = other_stat < stat
        value.append(f'- {"**" if is_better else ""}Split 2 Duration: '
                     f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')
        embed.add_field(name="Splits", value='\n'.join(value), inline=False)

        return embed


class SlothasorLogCollection(EncounterLogCollection):

    def report(self, other_log: SlothasorLog):
        embed = discord.Embed(title="Slothasor Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0x9ACD32)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        other_log.narcolepsy_phase(1)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({other_stat - stat:+.3f})'
                              f'{"**" if is_better else ""}')

        for p in range(1, 7):
            value = []
            stat = self.average(func=lambda l: l.narcolepsy_phase(p)[0])
            other_stat = other_log.narcolepsy_phase(p)[0]
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Duration: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            # stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
            # other_stat = other_log.uptime_25mfqa(f"Phase {p}")
            # is_better = other_stat > stat
            # value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
            #              f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            # value.append(" ")

            if p < 6:
                # TODO this is not safe from skipping a narcolepsy breakbar
                stat = self.average(func=lambda l: l.narcolepsy_phase(p)[1])
                other_stat = other_log.narcolepsy_phase(p)[1]
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Narcolepsy Duration: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                stat = self.average(func=lambda l: l.narcolepsy_phase(p)[2])
                other_stat = other_log.narcolepsy_phase(p)[2]
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Target Health at Breakbar: '
                             f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            embed.add_field(name=f"Phase {p}", value='\n'.join(value), inline=False)

        return embed


class MatthiasLogCollection(EncounterLogCollection):
    pass


class KeepConstructLogCollection(EncounterLogCollection):

    def best_magic_blast(self):
        results = self.sort(key=lambda l: sum(l.magic_blast()), reverse=True, status=LOG_SUCCESS)
        highest = results[0]
        results.sort()
        lowest = results[0]
        return lowest, highest

    def lowest_magic_blast(self):
        return self.sort(key=lambda l: sum(l.magic_blast()), status=LOG_SUCCESS)[0]

    def highest_magic_blast(self):
        return self.sort(key=lambda l: sum(l.magic_blast()), reverse=True, status=LOG_SUCCESS)[0]

    def report(self, other_log: KeepConstructLog):
        embed = discord.Embed(title="Keep Construct Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0xFF00FF)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({hms(other_stat - stat)})'
                              f'{"**" if is_better else ""}')

        for p in range(1, 4):
            value = []
            stat = self.average(func=lambda l: l.phase_duration(p))
            other_stat = other_log.phase_duration(p)
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Duration: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(f"Phase {p}"))
            other_stat = other_log.time_to_25mfqa(f"Phase {p}")
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Time to 25MFQA: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
            other_stat = other_log.uptime_25mfqa(f"Phase {p}")
            is_better = other_stat > stat
            value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
                         f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            value.append(" ")

            stat = self.average(func=lambda l: l.breakbar_time(p))
            other_stat = other_log.breakbar_time(p)
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Breakbar Start Time: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.breakbar_duration(p))
            other_stat = other_log.breakbar_duration(p)
            if other_stat is None:
                value.append(f'- {"**" if stat is not None else ""}Breakbar Duration: '
                             f'ignored{"**" if stat is not None else ""}')
            elif stat is None:
                value.append(f'- Breakbar Duration: '
                             f'{hms(other_stat)} (all recent logs ignored)')
            else:
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Breakbar Duration: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.kc_health_at_breakbar(p))
            other_stat = other_log.kc_health_at_breakbar(p)
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}KC Health at Breakbar: '
                         f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.duration_if_breakbar_skipped(p))
            other_stat = other_log.duration_if_breakbar_skipped(p)
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Expected Phase Duration if Breakbar Skipped: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            embed.add_field(name=f"Phase {p}", value='\n'.join(value), inline=False)

        value = []
        stat = self.sort(key=lambda l: l.num_breakbars_ignored())[0][0]
        other_stat = other_log.num_breakbars_ignored()
        is_better = other_stat > stat
        value.append(f'- {"**" if is_better else ""}Number of Breakbars Ignored: '
                     f'{other_stat} (max {stat}){"**" if is_better else ""}')

        # stat = self.average(func=lambda l: l.avg_statue_lifetimes())
        # other_stat = other_log.avg_statue_lifetimes()
        # is_better = other_stat < stat
        # value.append(f'- {"**" if is_better else ""}Avg Statue Lifetime: '
        #              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

        magic_blast = self.sort(key=lambda l: sum(l.magic_blast()), reverse=True)[0][0]
        other_magic_blast = sum(other_log.magic_blast())
        is_better = (other_magic_blast < magic_blast)
        value.append(f'- {"**" if is_better else ""}Magic Blast (orbs): '
                     f'{other_magic_blast} (min {magic_blast}){"**" if is_better else ""}')
        embed.add_field(name="Other Statistics", value='\n'.join(value), inline=False)

        return embed


class TwistedCastleLogCollection(EncounterLogCollection):

    def report(self, other_log: TwistedCastleLog):
        embed = discord.Embed(title="Twisted Castle Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0xFF00FF)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        prompt = {0: "Jump Button", 1: "Left Button", 2: "Center Button", 3: "Back Button", 4: "Triple Button"}
        output = []
        for b in range(5):
            stat = self.average(func=lambda l: l.button_time(b))
            other_stat = other_log.button_time(b)
            is_better = other_stat < stat
            output.append(f'{"**" if is_better else ""}{prompt[b]}: {hms(other_stat)} '
                          f'({other_stat - stat:+.3f}){"**" if is_better else ""}')

        embed.add_field(name=f"Buttons", inline=False,
                        value='\n'.join(output))
        return embed


class SoullessHorrorLogCollection(EncounterLogCollection):

    def movement(self):
        results = [(log.movement(), log) for log in self.logs]
        self.unload()
        return results

    def moving(self):
        results = [(log.moving(), log) for log in self.logs]
        self.unload()
        return results


class ConjuredAmalgamateLogCollection(EncounterLogCollection):

    def report(self, other_log: ConjuredAmalgamateLog):
        embed = discord.Embed(title="Conjured Amalgamate Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0xFF0000)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({hms(other_stat - stat)})'
                              f'{"**" if is_better else ""}')

        for p in range(1, 4):
            # value = []
            # stat = self.average(func=lambda l: l.arm_health_end(p, "Left Arm"))
            # other_stat = other_log.arm_health_end(p, "Left Arm")
            # is_better = other_stat < stat
            # value.append(f'- {"**" if is_better else ""}Left Arm Health: '
            #              f'{other_stat}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')
            #
            # stat = self.average(func=lambda l: l.arm_health_end(p, "Right Arm"))
            # other_stat = other_log.arm_health_end(p, "Right Arm")
            # is_better = other_stat < stat
            # value.append(f'- {"**" if is_better else ""}Right Arm Health: '
            #              f'{other_stat}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')
            #
            # embed.add_field(name=f"Arm Phase {p}", value='\n'.join(value), inline=False)

            value = []
            stat = self.average(func=lambda l: l.burn_phase(p)["duration"] / 1000)
            other_stat = other_log.burn_phase(p)["duration"] / 1000
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Duration: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(l.burn_phase_idx(p)))
            other_stat = other_log.time_to_25mfqa(other_log.burn_phase_idx(p))
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Time to 25MFQA: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(l.burn_phase_idx(p)))
            other_stat = other_log.uptime_25mfqa(other_log.burn_phase_idx(p))
            is_better = other_stat > stat
            value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
                         f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            value.append(' ')

            stat = self.average(func=lambda l: l.time_to_10fractured(p))
            other_stat = other_log.time_to_10fractured(p)
            if other_stat is None:
                value.append(f'- Time to 10 Fractured: did not reach 10 '
                             f'{f"(avg {stat})" if stat is not None else ""}')
            elif stat is None:
                value.append(f'- **Time to 10 Fractured: '
                             f'{hms(other_stat)} (all recent logs did not reach 10)**')
            else:
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Time to 10 Fractured: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.avg_fractured(p, "Conjured Amalgamate"))
            other_stat = other_log.avg_fractured(p, "Conjured Amalgamate")
            is_better = other_stat > stat
            value.append(f'- {"**" if is_better else ""}Avg. Fractured: '
                         f'{other_stat:.3f} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            embed.add_field(name=f"Burn Phase {p}", value='\n'.join(value), inline=False)

        return embed


class QadimLogCollection(EncounterLogCollection):

    def best_lamp_time(self, phase):
        return self.sort(key=lambda l: l.lamp_time(phase))[0]

    def report(self, other_log):
        output = ["Qadim", ""]
        for log in self.logs:
            output.append(f"{log.strf_date}: {log.url}")
        output.append(f"**{other_log.strf_date}: {other_log.url}**")
        output.append(" ")

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        output.append(f'{"**" if is_better else ""}Full Fight Duration: '
                      f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

        output.append(" ")

        for p in range(1, 4):
            output.append(f"Phase {p}")
            stat = self.average(func=lambda l: l.phase_duration(p))
            other_stat = other_log.phase_duration(p)
            is_better = other_stat < stat
            output.append(f'{"**" if is_better else ""}Duration: '
                          f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.lamp_time(p))
            other_stat = other_log.lamp_time(p)
            if other_stat is None:
                output.append(f'Lamp Time: N/A (N/A){"**" if is_better else ""}')
            else:
                is_better = other_stat < stat
                output.append(f'{"**" if is_better else ""}Lamp Time: '
                              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(f"Qadim P{p}"))
            other_stat = other_log.time_to_25mfqa(f"Qadim P{p}")
            is_better = other_stat < stat
            output.append(f'{"**" if is_better else ""}Time to 25MFQA: '
                          f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(f"Qadim P{p}"))
            other_stat = other_log.uptime_25mfqa(f"Qadim P{p}")
            is_better = other_stat > stat
            output.append(f'{"**" if is_better else ""}25MFQA Uptime: '
                          f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            output.append(" ")

        output.append("Boldface indicates improvement over the 4 week average")
        return '\n'.join(output)


class SabirLogCollection(EncounterLogCollection):

    def best_cc_time(self):
        results = []
        for log in self.success_logs:
            count = duration = 0
            for phase in log.data["phases"]:
                if phase["breakbarPhase"]:
                    count += 1
                    duration += phase["end"] - phase["start"] - 2
            if count >= 2:
                results.append((duration, log))

        self.unload()
        results.sort()
        return results[0]

    def report(self, other_log):
        embed = discord.Embed(title="Cardinal Sabir Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0xFF00FF)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({hms(other_stat - stat)})'
                              f'{"**" if is_better else ""}')

        for p in range(1, 4):
            value = []
            stat = self.average(func=lambda l: l.phase_duration(p))
            other_stat = other_log.phase_duration(p)
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Duration: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.time_to_25mfqa(f"Phase {p}"))
            other_stat = other_log.time_to_25mfqa(f"Phase {p}")
            is_better = other_stat < stat
            value.append(f'- {"**" if is_better else ""}Time to 25MFQA: '
                         f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
            other_stat = other_log.uptime_25mfqa(f"Phase {p}")
            is_better = other_stat > stat
            value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
                         f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            # stat = self.average(func=lambda l: l.breakbar_time(p))
            # other_stat = other_log.breakbar_time(p)
            # is_better = other_stat < stat
            # value.append(f'- {"**" if is_better else ""}Breakbar Start Time: '
            #              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

            if p < 3:
                value.append(" ")

                stat = self.average(func=lambda l: l.breakbar_duration(p))
                other_stat = other_log.breakbar_duration(p)
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Breakbar Duration: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                if p == 2:
                    stat = self.average(func=lambda l: l.sabir_health_at_shockwave())
                    other_stat = other_log.sabir_health_at_shockwave()
                    is_better = other_stat < stat
                    value.append(f'- {"**" if is_better else ""}Sabir Health at Shockwave: '
                                 f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

            embed.add_field(name=f"Phase {p}", value='\n'.join(value), inline=False)

        return embed


class QadimThePeerlessLogCollection(EncounterLogCollection):

    def report(self, other_log):
        embed = discord.Embed(title="Qadim the Peerless Monthly Average",
                              description="Parenthetical statistics are the difference between recent and average of "
                                          "the previous 4 weeks.  Boldface indicates improvement over this average.",
                              timestamp=datetime.datetime.utcnow(), colour=0xFF00FF)
        embed.set_thumbnail(url=other_log.encounter_icon)
        embed.description += "\n\n**Recent Logs**\n"
        embed.description += '\n'.join(f"{log.profession_emojis} [{log.strf_date}]({log.url})" for log in self.logs)
        embed.description += f"\n{other_log.profession_emojis} **[{other_log.strf_date}]({other_log.url})**"
        # value = [f'[{log.strf_date}]({log.url})' for log in self.logs]
        # value.append(f'**[{other_log.strf_date}]({other_log.url})**')
        # embed.add_field(name="Recent Logs", value='\n'.join(value), inline=False)

        stat = self.average(func=lambda l: l.duration)
        other_stat = other_log.duration
        is_better = other_stat < stat
        embed.add_field(name="Full Fight Duration", inline=False,
                        value=f'{"**" if is_better else ""}{hms(other_stat)} ({hms(other_stat - stat)})'
                              f'{"**" if is_better else ""}')

        try:
            for p in range(1, 7):
                value = []
                stat = self.average(func=lambda l: l.phase_duration(p))
                other_stat = other_log.phase_duration(p)
                is_better = other_stat < stat
                value.append(f'- {"**" if is_better else ""}Duration: '
                             f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                # stat = self.average(func=lambda l: l.time_to_25mfqa(f"Phase {p}"))
                # other_stat = other_log.time_to_25mfqa(f"Phase {p}")
                # is_better = other_stat < stat
                # value.append(f'- {"**" if is_better else ""}Time to 25MFQA: '
                #              f'{hms(other_stat)} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                stat = self.average(func=lambda l: l.uptime_25mfqa(f"Phase {p}"))
                other_stat = other_log.uptime_25mfqa(f"Phase {p}")
                is_better = other_stat > stat
                value.append(f'- {"**" if is_better else ""}25MFQA Uptime: '
                             f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

                if p < 6:
                    stat = self.average(func=lambda l: l.erratic_energy(p))
                    other_stat = other_log.erratic_energy(p)
                    is_better = other_stat > stat
                    value.append(f'- {"**" if is_better else ""}Avg Erratic Energy: '
                                 f'{other_stat:.3f} ({other_stat - stat:+.3f}){"**" if is_better else ""}')

                    stat = self.average(func=lambda l: l.kinetic_abundance_uptime(p))
                    other_stat = other_log.kinetic_abundance_uptime(p)
                    is_better = other_stat > stat
                    value.append(f'- {"**" if is_better else ""}Avg Kinetic Abundance Uptime: '
                                 f'{other_stat:.3f}% ({other_stat - stat:+.3f}%){"**" if is_better else ""}')

                embed.add_field(name=f"Phase {p}", value='\n'.join(value), inline=False)
        except IndexError:
            pass
        except KeyError:
            pass

        return embed


ENCOUNTER_CLS = {11: ValeGuardianLogCollection, 13: GorsevalLogCollection, 14: 'Sabetha the Saboteur',
                 21: SlothasorLogCollection, 22: 'Bandit Trio', 23: MatthiasLogCollection,
                 32: KeepConstructLogCollection,
                 33: TwistedCastleLogCollection, 34: 'Xera',
                 41: 'Cairn the Indomitable', 42: 'Mursaat Overseer', 43: 'Samarog', 44: 'Deimos',
                 51: SoullessHorrorLogCollection, 52: 'Desmina', 53: 'Broken King', 54: 'Eater of Souls', 55: 'Eye of Judgment',
                 56: 'Dhuum',
                 61: ConjuredAmalgamateLogCollection,
                 62: 'Nikare', 63: QadimLogCollection,
                 71: 'Cardinal Adina', 72: SabirLogCollection, 73: QadimThePeerlessLogCollection
                 }
