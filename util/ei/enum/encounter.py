from collections import namedtuple
from typing import Dict, Union

Encounter = namedtuple("Encounter", "name target_id wing_id")


ENCOUNTERS: Dict[Union[int, str], Encounter] = {
    15438: Encounter(name="Vale Guardian", target_id=15438, wing_id=11),
    15429: Encounter(name="Gorseval the Multifarious", target_id=15429, wing_id=13),
    15375: Encounter(name="Sabetha the Saboteur", target_id=15375, wing_id=14),

    16123: Encounter(name="Slothasor", target_id=16123, wing_id=21),
    16088: Encounter(name="Berg", target_id=16088, wing_id=22),
    16137: Encounter(name="Zane", target_id=16137, wing_id=22),
    16125: Encounter(name="Narella", target_id=16125, wing_id=22),
    16115: Encounter(name="Matthias Gabrel", target_id=16115, wing_id=23),

    16253: Encounter(name="McLeod the Silent", target_id=16253, wing_id=31),
    16235: Encounter(name="Keep Construct", target_id=16235, wing_id=32),
    16247: Encounter(name="Twisted Castle", target_id=16247, wing_id=33),
    16246: Encounter(name="Xera", target_id=16246, wing_id=34),

    17194: Encounter(name="Cairn", target_id=17194, wing_id=41),
    17172: Encounter(name="Mursaat Overseer", target_id=17172, wing_id=42),
    17188: Encounter(name="Samarog", target_id=17188, wing_id=43),
    17154: Encounter(name="Deimos", target_id=17154, wing_id=44),

    19767: Encounter(name="Soulless Horror", target_id=19767, wing_id=51),
    19828: Encounter(name="River of Souls", target_id=19828, wing_id=52),
    19691: Encounter(name="Broken King", target_id=19691, wing_id=53),
    19536: Encounter(name="Soul Eater", target_id=19536, wing_id=54),
    19651: Encounter(name="Eye of Judgement", target_id=19651, wing_id=55),
    19844: Encounter(name="Eye of Fate", target_id=19844, wing_id=55),
    19450: Encounter(name="Dhuum", target_id=19450, wing_id=56),

    43974: Encounter(name="Conjured Amalgamate", target_id=43974, wing_id=61),
    10142: Encounter(name="Conjured Amalgamate Right Arm", target_id=10142, wing_id=61),
    37464: Encounter(name="Conjured Amalgamate Left Arm", target_id=37464, wing_id=61),
    21105: Encounter(name="Nikare", target_id=21105, wing_id=62),
    21089: Encounter(name="Kenut", target_id=21089, wing_id=62),
    20934: Encounter(name="Qadim", target_id=20934, wing_id=63),

    22006: Encounter(name="Cardinal Adina", target_id=22006, wing_id=71),
    21964: Encounter(name="Cardinal Sabir", target_id=21964, wing_id=72),
    22000: Encounter(name="Qadim the Peerless", target_id=22000, wing_id=73),
}


for v in tuple(ENCOUNTERS.values()):
    ENCOUNTERS[v.wing_id] = v
    ENCOUNTERS[v.name] = v
    ENCOUNTERS[v.name.lower()] = v

ENCOUNTERS[12] = ENCOUNTERS["Spirit Run"] = Encounter(name="Spirit Run", target_id=None, wing_id=12)
ENCOUNTERS[22] = ENCOUNTERS["Bandit Trio"] = Encounter(name="Bandit Trio", target_id=(16088, 16137, 16125), wing_id=22)
ENCOUNTERS[55] = ENCOUNTERS["Eyes"] = Encounter(name="Eyes", target_id=(19651, 19844), wing_id=55)
ENCOUNTERS["Statue of Ice"] = ENCOUNTERS[53]
ENCOUNTERS["Statue of Death"] = ENCOUNTERS[54]
ENCOUNTERS["Statue of Darkness"] = ENCOUNTERS[55]
ENCOUNTERS[61] = ENCOUNTERS[43974]
ENCOUNTERS[62] = ENCOUNTERS["Twin Largos"] = Encounter(name="Twin Largos", target_id=(21105, 21089), wing_id=62)
ENCOUNTERS["Keep Construct CM"] = ENCOUNTERS[32]
ENCOUNTERS["Cairn CM"] = ENCOUNTERS[41]
ENCOUNTERS["Mursaat Overseer CM"] = ENCOUNTERS[42]
ENCOUNTERS["Soulless Horror CM"] = ENCOUNTERS[51]
ENCOUNTERS["Conjured Amalgamate CM"] = ENCOUNTERS[61]
ENCOUNTERS["Cardinal Sabir CM"] = ENCOUNTERS[72]
ENCOUNTER_NAMES = {11: 'Vale Guardian',
                   12: 'Spirit Run',
                   13: 'Gorseval the Multifarious',
                   14: 'Sabetha the Saboteur',
                   21: 'Slothasor',
                   22: 'Bandit Trio',
                   23: 'Matthias Gabrel',
                   32: 'Keep Construct',
                   33: 'Twisted Castle',
                   34: 'Xera',
                   41: 'Cairn the Indomitable',
                   42: 'Mursaat Overseer',
                   43: 'Samarog',
                   44: 'Deimos',
                   51: 'Soulless Horror',
                   52: 'Desmina',
                   53: 'Broken King',
                   54: 'Eater of Souls',
                   55: 'Eye of Judgment',
                   56: 'Dhuum',
                   61: 'Conjured Amalgamate',
                   62: 'Nikare',
                   63: 'Qadim',
                   71: 'Cardinal Adina',
                   72: 'Cardinal Sabir',
                   73: 'Qadim the Peerless',

                   "pre-sab": "Pre-Sabetha",
                   "pre-vg": "Pre-Vale Gaurdian",
                   "pre-kc": "Wing Break 3, Escort, Pre-Keep Construct",
                   "pre-tc": "Pre-Twisted Castle",
                   "pre-xera": "Pre-Xera",
                   "pre-sloth": "Wing Break 2, Pre-Slothasor",
                   "pre-trio": "Pre-Bandit Trio",
                   "pre-matt": "Pre-Matthias",
                   "pre-cairn": "Wing Break 4, Pre-Cairn",
                   "pre-mo": "Pre-Mursaat Overseer",
                   "pre-sam": "Pre-Samarog",
                   "pre-deimos": "Pre-Deimos",
                   "pre-adina": "Wing Break 7, Pre-Adina",
                   "pre-sabir": "Pre-Sabir",
                   "pre-qtp": "Pre-Qadim the Peerless",
                   "pre-ca": "Wing Break 6, Pre-Conjured Amalgamate",
                   "pre-largos": "Pre-Largos",
                   "pre-qadim": "Pre-Qadim",
                   "pre-sh": "Wing Break 5, Pre-Soulless Horror",
                   "pre-river": "Pre-River of Souls",
                   "pre-statues": "Pre-Statues",
                   "statues": "Statues",
                   "pre-dhuum": "Pre-Dhuum",

                   (13, 14): "Pre-Sabetha",
                   (14, 11): "Pre-Vale Gaurdian",
                   (11, 32): "Wing Break 3, Escort, Pre-Keep Construct",
                   (32, 33): "Pre-Twisted Castle",
                   (33, 34): "Pre-Xera",
                   (34, 21): "Wing Break 2, Pre-Slothasor",
                   (21, 22): "Pre-Bandit Trio",
                   (22, 23): "Pre-Matthias",
                   (23, 41): "Wing Break 4, Pre-Cairn",
                   (41, 42): "Pre-Mursaat Overseer",
                   (42, 43): "Pre-Samarog",
                   (43, 44): "Pre-Deimos",
                   (44, 71): "Wing Break 7, Pre-Adina",
                   (71, 72): "Pre-Sabir",
                   (72, 73): "Pre-Qadim the Peerless",
                   (73, 61): "Wing Break 6, Pre-Conjured Amalgamate",
                   (61, 62): "Pre-Largos",
                   (62, 63): "Pre-Qadim",
                   (63, 51): "Wing Break 5, Pre-Soulless Horror",
                   (51, 52): "Pre-River of Souls",
                   (52, 53): "Pre-Broken King",
                   (53, 54): "Pre-Eater of Souls",
                   (54, 55): "Pre-Eyes",
                   (55, 56): "Pre-Dhuum"
                   }

ENCOUNTER_IDS = {"Vale Guardian": 11,
                 "Gorseval the Multifarious": 13,
                 "Sabetha the Saboteur": 14,
                 "Slothasor": 21,
                 "Bandit Trio": 22,
                 "Matthias Gabrel": 23,
                 "Keep Construct": 32,
                 "Keep Construct CM": 32,
                 "Twisted Castle": 33,
                 "Xera": 34,
                 "Cairn": 41,
                 "Cairn CM": 41,
                 "Cairn the Indomitable": 41,
                 "Cairn the Indomitable CM": 41,
                 "Mursaat Overseer": 42,
                 "Mursaat Overseer CM": 42,
                 "Samarog": 43,
                 "Samarog CM": 43,
                 "Deimos": 44,
                 "Deimos CM": 44,
                 "Soulless Horror": 51,
                 "Soulless Horror CM": 51,
                 "River of Souls": 52,
                 "Statue of Ice": 53,
                 "Statue of Death": 54,
                 "Statue of Darkness": 55,
                 "Dhuum": 56,
                 "Dhuum CM": 56,
                 "Conjured Amalgamate": 61,
                 "Conjured Amalgamate CM": 61,
                 "Twin Largos": 62,
                 "Twin Largos CM": 62,
                 "Qadim": 63,
                 "Qadim CM": 63,
                 "Cardinal Adina": 71,
                 "Cardinal Adina CM": 71,
                 "Cardinal Sabir": 72,
                 "Cardinal Sabir CM": 72,
                 "Qadim the Peerless": 73,
                 "Qadim the Peerless CM": 73,
                 }
