import json
import os
from collections import namedtuple

# TODO double check, this might be 5 hours ahead
from typing import Dict

from config import cfg

RESET_TIMESTAMP = 372600


BALANCE_PATCHES = {"20210817": 118697,
                   "20210511": 115190,
                   }


Skill = namedtuple("Skill", "name id type profession")
SKILLS: Dict[int, Skill] = {}
with open(os.path.join(cfg["data_dir"], "skills.json"), 'r') as f:
    for skill in json.load(f):
        SKILLS[skill["id"]] = Skill(**skill)

# with open(os.path.join(cfg["data_dir"], "buff.dat"), 'r') as f:
#     BUFF_IDS = dict(map(lambda x: (int(x[1]), x[0]), (line.split(',', 1) for line in f.readlines())))
#
#
def save_mappings():
    pass
#     with open(os.path.join(cfg["data_dir"], "buff.dat"), 'w') as _f:
#         _f.writelines(sorted(f"{n},{i}\n" for i, n in BUFF_IDS.items()))
BUFF_IDS = {}


MAIN_HAND = ["Axe", "Dagger", "Mace", "Pistol", "Scepter", "Sword"]
OFF_HAND = ["Axe", "Dagger", "Focus", "Mace", "Pistol", "Shield", "Sword", "Torch", "Warhorn"]
TWO_HAND = ["Greatsword", "Hammer", "Longbow", "Rifle", "Shortbow", "Staff"]


PASSIVE_SIGNET_IDS = {
    # BUFF:SKILL
    13047: 13046,  # Assassin's Signet
    13061: 13062,  # Signet of Agility,
    14444: 14404,  # Signet of Might,
    # 10610: ,  # Signet of Undeath (Passive),
     9100:  9151,  # Signet of Wrath,
     9092:  9093,  # Bane Signet
}


DUAL_SKILLS = {
    12593: 12497,  # Cold Snap, Frost Spirit
    12592: 12498,  # Solar Flare, Sun Spirit
    12594: 12493,  # Call Lightning, Storm Spirit
    12595: 12495,  # Quicksand, Stone Spirit
    29558: 30448,  # Glyph of the Tides
    30693: 30868,  # Palm Strike, Fist Flurry
}


NOT_SLOTTED_SKILLS = [44677]


LEGEND_BOON_IDS = {
    "Glint": 27732,
    "Jalis": -8128,
    "Kalla": 44272,
    "Mallyx": 27928,
    "Shiro": 27890,
    "Ventari": 27972,
}

BOON_IDS = {"might": 740,
            "fury": 725,
            "quickness": 1187,
            "alacrity": 30328,
            "protection": 717,
            "regeneration": 718,
            "vigor": 726,
            "aegis": 743,
            "stability": 1122,
            "swiftness": 719,
            "resolution": 873,

            "confusion": 861,
            "torment": 19426,
            "bleeding": 736,
            "burning": 737,
            "poison": 723,
            "blind": 720,
            "crippled": 721,
            "fear": 791,
            "weakness": 742,

            "banner_of_strength": 14417,
            "banner_of_discipline": 14449,
            "sun_spirit": 50413,
            "spotter": 14055,
            "stone_spirit": 50415}


