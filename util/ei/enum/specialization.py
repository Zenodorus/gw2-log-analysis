import json
import os
from collections import namedtuple
from typing import Dict, Union, List, Optional, Any

from config import cfg
from util.ei.enum import Skill, TWO_HAND, SKILLS

Profession = namedtuple("Profession", "name base id emoji health")
Trait = namedtuple("Trait", "name id tier order slot specialization icon")
Specialization = namedtuple("Specialization", "name id profession elite minor_traits major_traits weapon_trait icon")


# https://wiki.guildwars2.com/wiki/Category:Elite_specialization_icons
PROFESSIONS: Dict[str, Profession] = {
    "Warrior":      Profession(name="Warrior", base="Warrior", id=10, emoji="<:warrior:889272702127468634>",
                               health=19212),
    "Berserker":    Profession(name="Berserker", base="Warrior", id=11, emoji="<:berserker:889273057317892137>",
                               health=19212),
    "Spellbreaker": Profession(name="Spellbreaker", base="Warrior", id=12, emoji="<:spellbreaker:889272880746078251>",
                               health=19212),
    "Bladesworn":   Profession(name="Bladesworn", base="Warrior", id=13, emoji="<:bladesworn:889273043736731678>",
                               health=19212),

    "Revenant":     Profession(name="Revenant", base="Revenant", id=20, emoji="<:revenant:889272724160151564>",
                               health=15922),
    "Herald":       Profession(name="Herald", base="Revenant", id=21, emoji="<:herald:889272955522146384>",
                               health=15922),
    "Renegade":     Profession(name="Renegade", base="Revenant", id=22, emoji="<:renegade:889272918620635147>",
                               health=15922),
    "Vindicator":   Profession(name="Vindicator", base="Revenant", id=23, emoji="<:vindicator:889272863876591658>",
                               health=15922),

    "Guardian":     Profession(name="Guardian", base="Guardian", id=30, emoji="<:guardian:889272822503989248>",
                               health=11645),
    "Dragonhunter": Profession(name="Dragonhunter", base="Guardian", id=31, emoji="<:dragonhunter:889272996219469875>",
                               health=11645),
    "Firebrand":    Profession(name="Firebrand", base="Guardian", id=32, emoji="<:firebrand:889272975654780979>",
                               health=11645),
    "Willbender":   Profession(name="Willbender", base="Guardian", id=33, emoji="<:willbender:889272832108925010>",
                               health=11645),

    "Thief":        Profession(name="Thief", base="Thief", id=40, emoji="<:thief:889272714030891018>",
                               health=11645),
    "Daredevil":    Profession(name="Daredevil", base="Thief", id=41, emoji="<:daredevil:889273020168933496>",
                               health=11645),
    "Deadeye":      Profession(name="Deadeye", base="Thief", id=42, emoji="<:deadeye:889273005203669053>",
                               health=11645),

    "Ranger":       Profession(name="Ranger", base="Ranger", id=50, emoji="<:ranger:889272734494887976>",
                               health=15922),
    "Druid":        Profession(name="Druid", base="Ranger", id=51, emoji="<:druid:889272984764841984>",
                               health=15922),
    "Soulbeast":    Profession(name="Soulbeast", base="Ranger", id=52, emoji="<:soulbeast:889272890976002058>",
                               health=15922),

    "Engineer":     Profession(name="Engineer", base="Engineer", id=60, emoji="<:engineer:889272813784031253>",
                               health=15922),
    "Scrapper":     Profession(name="Scrapper", base="Engineer", id=61, emoji="<:scrapper:889272899662385172>",
                               health=15922),
    "Holosmith":    Profession(name="Holosmith", base="Engineer", id=62, emoji="<:holosmith:889272947506815016>",
                               health=15922),

    "Necromancer":  Profession(name="Necromancer", base="Necromancer", id=70,
                               emoji="<:necromancer:889272742115962971>", health=19212),
    "Reaper":       Profession(name="Reaper", base="Necromancer", id=71, emoji="<:reaper:889272927961362462>",
                               health=19212),
    "Scourge":      Profession(name="Scourge", base="Necromancer", id=72, emoji="<:scourge:889272908801785917>",
                               health=19212),
    "Harbinger":    Profession(name="Harbinger", base="Necromancer", id=73, emoji="<:harbinger:889272965462654986>",
                               health=19212),

    "Mesmer":       Profession(name="Mesmer", base="Mesmer", id=80, emoji="<:mesmer:889272783350136843>",
                               health=15922),
    "Chronomancer": Profession(name="Chronomancer", base="Mesmer", id=81, emoji="<:chronomancer:889273035767574538>",
                               health=15922),
    "Mirage":       Profession(name="Mirage", base="Mesmer", id=82, emoji="<:mirage:889272938195456010>",
                               health=15922),
    "Virtuoso":     Profession(name="Virtuoso", base="Mesmer", id=83, emoji="<:virtuoso:889272855353770024>",
                               health=15922),

    "Elementalist": Profession(name="Elementalist", base="Elementalist", id=90,
                               emoji="<:elementalist:889272794146291712>", health=11645),
    "Tempest":      Profession(name="Tempest", base="Elementalist", id=91, emoji="<:tempest:889272872563011594>",
                               health=11645),
    "Weaver":       Profession(name="Weaver", base="Elementalist", id=92, emoji="<:weaver:889272845300039772>",
                               health=11645),
    "Catalyst":     Profession(name="Catalyst", base="Elementalist", id=93, emoji="", health=11645),
}


with open(os.path.join(cfg["data_dir"], "specializations.json"), 'r') as f:
    SPECIALIZATIONS: Dict[Union[str, int], Specialization] = {}
    keys = set("name id profession elite minor_traits major_traits weapon_trait icon".split(' '))
    for s in json.load(f):
        for k in ("background", "profession_icon_big", "profession_icon"):
            if k in s:
                del s[k]
        if "weapon_trait" not in s:
            s["weapon_trait"] = None
        assert set(s) == keys
        s = Specialization(**s)

        SPECIALIZATIONS[s.id] = s
        SPECIALIZATIONS[s.name] = s


with open(os.path.join(cfg["data_dir"], "traits.json"), 'r') as f:
    TRAITS: Dict[Union[str, int], Trait] = {}
    for t in json.load(f):
        t = Trait(**t)
        TRAITS[t.id] = t
        if t.tier == 0:
            assert t.name.endswith("Proficiency")
        # This allows for the major Zephyr's Speed in ranger to overwrite minor Zephyr's Speed in elementalist
        elif t.slot == "Major" and t.name:
            TRAITS[t.name] = t
        elif t.name not in TRAITS and t.name:
            TRAITS[t.name] = t


class BuildError(Exception):
    pass


class Build(object):
    __slots__ = ("_profession", "_traits", "_weapons", "_skills", "_damage")

    def __init__(self, profession):
        self._profession = profession
        self._traits: List[Dict[str, Any]] = [{"specialization": None, "major": [None, None, None]},
                                              {"specialization": None, "major": [None, None, None]},
                                              {"specialization": None, "major": [None, None, None]}]
        self._weapons: List[Optional[List[str]]] = [None, None]
        if PROFESSIONS[self._profession].base == "Revenant":
            self._skills: List[Optional[Skill]] = [None, None]
        else:
            self._skills: List[Optional[Skill]] = [None, None, None, None, None]
        self._damage: Optional[str] = None

    @classmethod
    def from_json(cls, profession, j) -> "Build":
        build = Build(profession)
        for line in filter(None, j["traits"]):
            build.add_specialization(line["specialization"])
            for trait in filter(None, line["major"]):
                build.add_trait(trait)
        for w in filter(None, j["weapons"]):
            build.add_weapon(*w)
        for _s in filter(None, j["skills"]):
            build.add_skill(_s)
        build.damage = j["damage"]
        return build

    @property
    def damage(self) -> Optional[str]:
        return self._damage

    @damage.setter
    def damage(self, value):
        assert value in ("Power", "Condition", "Hybrid", "", None)
        self._damage = value

    # Weapons

    @property
    def weapons(self):
        return self._weapons

    def add_weapon(self, main_hand, off_hand=None):
        if main_hand in TWO_HAND:
            weapon = [main_hand]
            if off_hand is not None:
                raise BuildError(f"Specified a two handed weapon for main hand along with an off hand weapon")
        else:
            weapon = [main_hand, off_hand]

        if self._weapons[0] is None:
            self._weapons[0] = weapon
        elif self._weapons[1] is None:
            self._weapons[1] = weapon
        else:
            raise BuildError(f"Player already has two weapon sets specified")

    # Traits

    @property
    def traits(self):
        return self._traits

    def add_specialization(self, specialization: Union[int, str]):
        specialization = SPECIALIZATIONS[specialization]

        if specialization.elite:
            if self._traits[2]["specialization"] is not None and self._traits[2]["specialization"] != specialization:
                raise BuildError(f"Player already has a specified elite specialization")
            self._traits[2]["specialization"] = specialization
            return self._traits[2]

        for i in range(3):
            if self._traits[i]["specialization"] is None or self._traits[i]["specialization"] == specialization:
                self._traits[i]["specialization"] = specialization
                return self._traits[i]

        raise BuildError(f"Player has 3 specializations specified already")

    def add_trait(self, trait: Union[int, str, Trait]):
        if not isinstance(trait, Trait):
            trait = TRAITS[trait]
        line = self.add_specialization(trait.specialization)

        if trait.slot == "Major":
            if line["major"][trait.tier - 1] is not None and line["major"][trait.tier - 1] != trait:
                raise BuildError(f"Player already has a tier {trait.tier} major trait specified")
            line["major"][trait.tier - 1] = trait

    # Skills

    @property
    def skills(self) -> List[Optional[Skill]]:
        return self._skills

    def add_skill(self, skill: Union[int, Skill]):
        if isinstance(skill, int):
            skill = SKILLS[skill]

        if PROFESSIONS[self._profession].base == "Revenant":
            if skill.type == "Profession" and skill.profession == "Revenant":
                if not (skill.name.startswith("Legendary") and skill.name.endswith("Stance")):
                    raise BuildError(f"Revenent profession skill cannot be added in this way")
                if self._skills[0] is None:
                    self._skills[0] = skill
                elif self._skills[1] is None:
                    self._skills[1] = skill
                else:
                    pass
                    # raise BuildError(f"More than 2 legends specified for a player")
            else:
                raise BuildError(f"Inappropriate skill specified for Revenant")
        else:
            if skill.type == "Heal":
                if self._skills[0] is not None and self._skills[0] != skill:
                    raise BuildError(f"Player already has another heal skill")
                else:
                    self._skills[0] = skill
            elif skill.type == "Utility":
                if self._skills[1] is None:
                    self._skills[1] = skill
                elif self._skills[2] is None:
                    self._skills[2] = skill
                elif self._skills[3] is None:
                    self._skills[3] = skill
                elif skill not in self._skills[1:4]:
                    raise BuildError(f"Player already has 3 utility skills")
            elif skill.type == "Elite":
                if self._skills[4] is not None and self._skills[4] != skill:
                    raise BuildError(f"Player already has another elite skill")
                else:
                    self._skills[4] = skill
            else:
                raise BuildError(f"Builds cannot hold skills not on a skill bar")

    @staticmethod
    def _cmp_weapon(s_w, o_w):
        if s_w is None:
            return True
        elif o_w is None:
            return False
        elif len(s_w) != len(o_w):
            return False
        for i in range(len(s_w)):
            if s_w[i] is not None and s_w[i] != o_w[i]:
                return False
        return True

    def __lt__(self, other: "Build"):
        """
        Checks if elements specified in self are in other.

        :param other:
        :return:
        """
        for s in self._skills:
            if s is not None and s not in other.skills:
                return False

        self_specs = set(map(lambda x: x.id, filter(None, (line["specialization"] for line in self._traits))))
        other_specs = set(map(lambda x: x.id, filter(None, (line["specialization"] for line in other._traits))))
        if not self_specs <= other_specs:
            return False

        self_traits = self._traits[0]["major"] + self._traits[1]["major"] + self._traits[2]["major"]
        other_traits = other._traits[0]["major"] + other._traits[1]["major"] + other._traits[2]["major"]
        if not set(filter(None, self_traits)) <= set(filter(None, other_traits)):
            return False

        if self._damage is not None and self._damage != other.damage:
            return False

        if (self._cmp_weapon(self._weapons[0], other._weapons[0]) and
                self._cmp_weapon(self._weapons[1], other._weapons[1])):
            return True
        elif (self._cmp_weapon(self._weapons[1], other._weapons[0]) and
              self._cmp_weapon(self._weapons[0], other._weapons[1])):
            return True
        else:
            return False

    def __sub__(self, other: "Build"):
        """
        Returns a Build that has everything in self but not in other.

        :param other:
        :return:
        """
        diff = Build(self._profession)
        for s in self._skills:
            if s is not None and s not in other.skills:
                diff.add_skill(s)

        self_traits = self._traits[0]["major"] + self._traits[1]["major"] + self._traits[2]["major"]
        other_traits = other._traits[0]["major"] + other._traits[1]["major"] + other._traits[2]["major"]
        for trait in set(filter(None, self_traits)) - set(filter(None, other_traits)):
            diff.add_trait(trait)

        if self._damage is not None and self._damage != other.damage:
            diff.damage = self._damage

        if (self._cmp_weapon(self._weapons[0], other._weapons[0]) and
                self._cmp_weapon(self._weapons[1], other._weapons[1])):
            pass
        elif (self._cmp_weapon(self._weapons[1], other._weapons[0]) and
              self._cmp_weapon(self._weapons[0], other._weapons[1])):
            pass
        else:
            if self._weapons[0] is not None:
                diff.add_weapon(*self._weapons[0])
            if self._weapons[1] is not None:
                diff.add_weapon(*self._weapons[1])

        return diff


with open(os.path.join(cfg["data_dir"], "builds.json"), 'r') as f:
    BUILDS = []
    for b in json.load(f):
        if b["name"]:
            b["build"] = Build.from_json(b["profession"], b["build"])
            b["required"] = Build.from_json(b["profession"], b["required"])
            for b2 in BUILDS:
                if b["required"] < b2["build"]:
                    raise BuildError(f"Known builds are not mutually exclusive")
            BUILDS.append(b)
