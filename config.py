import json
import os
import uuid
import asyncio

from util import DateTimeDecoder, DateTimeEncoder


class Config(object):

    def __init__(self, filename):
        self._filename = filename
        self._loop = asyncio.get_event_loop()
        self._lock = asyncio.Lock()
        self._load()

    def _load(self):
        try:
            with open(self._filename, 'r') as f:
                self._data = json.load(f, cls=DateTimeDecoder)
        except FileNotFoundError:
            self._data = {}

    async def load(self):
        async with self._lock:
            await self._loop.run_in_executor(None, self._load)

    def _dump(self):
        temp = f"{uuid.uuid4()}-{self._filename}.tmp"
        with open(temp, 'w', encoding='utf-8') as tmp:
            json.dump(self._data.copy(), tmp, cls=DateTimeEncoder, indent=2)

        # atomically move the file
        os.replace(temp, self._filename)

    async def save(self):
        async with self._lock:
            await self._loop.run_in_executor(None, self._dump)

    def get(self, key, *args):
        """Retrieves a config entry."""
        return self._data.get(str(key), *args)

    async def put(self, key, value):
        """Edits a config entry."""
        self._data[str(key)] = value
        await self.save()

    async def remove(self, key):
        """Removes a config entry."""
        del self._data[str(key)]
        await self.save()

    def __contains__(self, item):
        return str(item) in self._data

    def __getitem__(self, item):
        return self._data[str(item)]

    def __setitem__(self, key, value):
        self._data[str(key)] = value

    def __len__(self):
        return len(self._data)


cfg = Config("config.json")
